..  _selections:

Auteur : Paul Passy

Licence : |cc_by_nc_sa|

.. |cc_by_nc_sa| image:: figures/Cc-by-nc-sa_icone.png
              :width: 80 px

Sélections attributaires et spatiales
=======================================

Le point fondamental des SIG est de permettre des sélections attributaires et spatiales sur les données vecteur. Ce croisement d'information est indispensable à toute projet de géomatique. Nous présenterons ici ces deux grands types de sélection de façon théorique et pratique avec différents outils.

.. contents:: Table des matières
    :local:

..  _selection-attributaire:

Sélection attributaire
-------------------------

La sélection d'entités basée sur des critères fondés sur les attributs d'une couche est un aspect fondamental des SIG. C'est même presque la raison d'être des SIG. Ces sélections peuvent être très simples ou très complexes. Selon les besoins et les connaissances de l'utilisateur, les sélections sont un outil puissant. L'interrogation des données attributaires se fait selon le langage SQL. Ce langage a l'avantage d'être simple pour des requêtes basiques mais peut se complexifier pour mener à bien des requêtes complexes.

Ce type de requêtes est qualifié de *requêtes attributaires*, à mettre en parallèle des *requêtes spatiales* présentées dans une autre section.

..  _selection-attributaire-qgis:

Requêtes attributaires dans QGIS
************************************
Version de QGIS : 3.16.1

Dans QGIS, les requêtes attributaires se font simplement via le menu *Sélection par expression*. Ce menu est accessible depuis la barre d'outils de *sélection* (:numref:`select-expr`). Il faut au préalable avoir sélectionné la couche à interroger dans le panneau de couches. Dans cet exemple, nous travaillerons sur la couche des communes des Hauts-de-Seine (92) *communes_92.gpkg*.

.. figure:: figures/fig_selection_par_expression.png
    :width: 25em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: select-expr
    
    Ouverture du menu de sélection par expression.

Plus précisément, le menu à utiliser se nomme *Sélectionner des entités à l'aide d'une expression...* dont l'icône ressemble à ça |icone-select-expr|. Une fois ce menu sélectionné, la fenêtre de *Sélection par expression* s'affiche (:numref:`select-expr-vide`).

.. figure:: figures/fen_select_expr_vide.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: select-expr-vide
    
    Le menu de sélection par expression.

Dans ce menu, nous retrouvons sur la gauche un panneau *Expression*. C'est dans ce panneau que nous entrerons notre expression en SQL nous permettant de faire notre requête. Dans le panneau central, nous trouvons des menus déroulants contenant des fonctions qui vont nous servir pour construire notre requête. Ces fonctions sont rangées par grand thème : celles qui permettent de manipuler le texte (*Chaîne de caractères*), celles portant sur les conversions de type (*Conversions*), celles permettant de requêter sur la géométrie (*Géométrie*), etc

Un menu déroulant que nous utiliserons tout le temps est celui permettant d'accéder aux champs de la couche *Champs et valeurs* (:numref:`select-expr-champs`).

.. figure:: figures/fen_select_expr_champs.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: select-expr-champs
    
    Liste des champs de la couche et de leurs valeurs.

En déroulant ce menu, nous accédons à la liste des champs présents dans cette couche. Nous pouvons ensuite cliquer sur un champ et demander à QGIS de nous lister toutes les valeurs uniques de ce champ ou un échantillon de dix valeurs via le panneau en bas à droite. Cette fonctionnalité s'avère très intéressante pour éviter de faire des fautes lorsqu'on requête sur un champ texte. Par exemple, dans notre cas, ça nous évite de chercher *Boulogne-Billancourt*, *boulogne billancourt*, *boulogne-billancourt* ... mais d'avoir directement accès à l'orthographe tel qu'enregistré dans la couche.

.. warning::
	La casse d'un mot (i.e. le fait qu'il soit écrit en majuscules ou en minuscules) est important. Ainsi *Malakoff* et *MALAKOFF* sont deux mots différents.

Il faut être prudent avec cette fonctionnalité de lister toutes les valeurs uniques. Sur un champ contenant des milliers de valeurs différentes (typiquement un champ numérique) QGIS va avoir du mal à les lister.

Requête basique
++++++++++++++++++
Nous allons commencer par construire une requête très basique mais fondamentale. Nous allons sélectionner les communes qui ont plus de 50 000 habitants. Nous allons donc interroger le champ *Population*.

Pour cela, dans le panneau central, dans le menu *Champs et valeurs*, nous double cliquons sur *POPULATION*. Le champ apparaît alors dans le panneau de gauche encadré de guillemets. Les guillemets font partie de la syntaxe SQL. Elles sont un marqueur pour bien préciser que le texte entre ces guillemets renvoie à un nom de champ.

Ensuite, de façon tout à fait naturel, nous entrons la suite de l'expression : ``> 50000`` (:numref:`select-pop-50000`).

.. figure:: figures/fen_select_expr_pop_50000.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: select-pop-50000
    
    Requête permettant de sélectionner les communes de plus de 50 000 habitants.

Il n'y a plus qu'à cliquer sur *Sélectionner des entités*. Les communes qui répondent à ce critère apparaissent en jaune (:numref:`communes-pop-50000`).

.. figure:: figures/fig_communes_pop_50000.png
    :width: 10em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: communes-pop-50000
    
    Résultat de la requête, les communes correspondant au critère apparaissent en jaune.

En ouvrant la table attributaire, les lignes correspondantes aux communes sélectionnées sont surlignées en bleu. Nous avons également le compte de communes sélectionnées en haut du cadre de la fenêtre de cette table, ici *12* (:numref:`communes-pop-50000-table`).

.. figure:: figures/fen_communes_pop_50000_table.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: communes-pop-50000-table
    
    12 communes répondent au critère de sélection.

.. note::
	Pour déselectionner les entités, le plus simple est de cliquer sur l'icône *Déselectionner toutes les entités* |icone-deselect|.

Requête avec des booléens
++++++++++++++++++++++++++
Il est souvent nécessaire d'inclure dans notre requête des opérateurs booléens comme *Et*, *Ou* ... Par exemple, nous pouvons avoir besoin de ne sélectionner que les communes dont la population est comprise entre 30000 et 50000 habitants. Dans ce cas, l'opérateur *Et* (*And*) sera nécessaire. Notre requête s'écrira alors ``"POPULATION" > 30000  AND  "POPULATION" < 50000`` (:numref:`communes-bool`).

.. figure:: figures/fen_select_expr_bool.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: communes-bool
    
    Sélection utilisant un booléen.

Comme précédemment, les communes répondant à ce double critère apparaissent en jaune.

Une fonctionnalité intéressante de QGIS est de se servir d'une fonction booléenne un peu "cachée". C'est une sorte de fonction booléenne par bricolage. Imaginons que nous souhaitions sélectionner les communes précédentes et aussi la commune de Vaucresson (qui n'entre pas dans le critère). La manière élégante et SQL de procéder est d'entrer la requête suivante ``("POPULATION" > 30000  AND  "POPULATION" < 50000) or "NOM_COM" = 'VAUCRESSON'``. Nous remarquons l'ajout de parenthèse autour de la première condition et l'apparition du booléen *OR* suivi de la seconde condition. L'exécution de cette requête donne bien le résultat attendu.

Une manière moins élégante mais parfois plus rapide est de se servir de la fonctionnalité *Ajouter à la sélection actuelle*. Concrètement, dans un premier temps, il suffit d'entrer seulement la requête principale dans le panneau *Expression* ``"POPULATION" > 30000  AND  "POPULATION" < 50000`` puis de l'exécuter normalement en cliquant sur *Sélectionner des entités*. Une fois les entités sélectionnées, nous les gardons bien sélectionnées et nous entrons maintenant la requête secondaire ``"NOM_COM" = 'VAUCRESSON'``. Mais au lieu de cliquer sur *Sélectionner des entités*, nous cliquons sur *Ajouter à la sélection actuelle* (:numref:`ajout-selection`).

.. figure:: figures/fen_ajouter_a_la_selection.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: ajout-selection
    
    Ajouter un résultat de sélection à une sélection précédente.

Une fois cette seconde requête exécutée de cette façon, la commune de Vaucresson est bien ajoutée à notre précédente sélection.

Un point encore plus intéressant est que ce principe peut être utilisé pour des sélections du type : *sélectionne moi toutes ces communes sauf celle-ci*. Le *sauf* peut être fastidieux à retranscrire en SQL. Ici, par exemple, nous allons sélectionner nos communes comprises entre 30000 et 50000 habitants sauf la commune de Puteaux qui répond pourtant au critère. La façon élégante serait d'écrire la requête suivante ``("POPULATION" > 30000  AND  "POPULATION" < 50000) AND "NOM_COM" != 'PUTEAUX'``. Nous retrouvons nos parenthèses autour de la requête principale et notre requête secondaire mais cette fois-ci liée par un *AND* et présentant l'opérateur *!=* qui signifie *différent de*. Notre requête se lit donc comme suit : *Sélectionne moi les communes qui ont plus de 30000 habitants et moins de 50000 habitants et dont le nom n'est pas Puteaux*. Ça reste gérable mais on sent que ça peut vite se compliquer.

Dans ce cas, un moyen simple est de commencer par exécuter la requête principale seule. Une fois les communes sélectionnées, nous retournons dans le menu de sélection par expression et nous y entrons la requête simple ``"NOM_COM" = 'PUTEAUX'``. Puis, au lieu de cliquer sur *Sélectionner des entités*, nous cliquons sur *Supprimer de la sélection actuelle* (:numref:`selection-sauf`).

.. figure:: figures/fen_selection_sauf.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: selection-sauf
    
    Enlever une entité de la sélection précédente.

Ainsi, la commune qui répond au second critère est enlevée de la sélection précédente. C'est donc un moyen de faire des requêtes booléennes sans trop de gymnastique mentale.

Requête avec des opérateurs textuels
++++++++++++++++++++++++++++++++++++++++
Il est possible de faire des requêtes un peu poussées sur des champs de type *String* (*Texte*). Pour cet exemple, nous allons charger la couche des communes d'Île-de-France "*communes_IDF.gpkg*". Nous allons sélectionner les communes dont le code INSEE commence par *95*. Nous allons donc extraire les communes du Val-d'Oise. Ce code se trouve dans le champ *INSEE_COM*. Nous ouvrons le menu de sélection par expression. Nous allons voir comment dire en SQL "*Sélectionne moi toutes les communes dont le champ INSEE_COM commence par 95*". La syntaxe ne se devine pas. 

La requête s'écrit ainsi ``"INSEE_COM" like '95%'``. Les deux choses importantes à saisir sont la présence de mot clef *like* et du symbole *%*. *Like* est utilisé dans le cas d'une comparaison pour un champ texte. Le *%* est un caractère joker qui signifie *n'importe quel caractère*. Ainsi *95%* équivaut à un texte commençant par *95* suivi de n'importe quel caractère (un ou plusieurs, sans importance) (:numref:`selection-95`).

.. figure:: figures/fen_select_95_joker.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: selection-95
    
    Sélectionner les communes dont le code commence par 95.

Dans la même logique, il est possible de sélectionner les communes dont le nom est composé, c'est-à-dire qui contiennent le caractère *-*. La requête sera alors ``"NOM_COM" like '%-%'``. Nous sélectionnons ici les communes dont le nom commence par n'importe quel caractère suivi d'un *-* puis suivi de n'importe quel caractère. Nous nous apercevons que quasiment la moitié des communes d'Îlde-de-France présente un nom composé !

Il est également possible de sélectionner des entités dont un champ texte fait partie d'une liste donnée. Par exemple si nous souhaitons sélectionner les communes de Cergy, Pontoise, Malakoff et Bobigny, nous sommes, de prime abord, tentés d'écrire la requête suivante : ``"NOM_COM" = 'CERGY' or  "NOM_COM" = 'PONTOISE' or  "NOM_COM" = 'MALAKOFF' or  "NOM_COM" = 'BOBIGNY'``. Cette requête va parfaitement fonctionner mais sa lisibilité laisse à désirer. Nos chances d'oublier une *"* ou un *=* sont importantes.

Il existe un moyen plus simple de faire ce genre de sélection. Nous pouvons nous servir du mot clef SQL *in*, de la façon suivante : ``"NOM_COM" in ('CERGY', 'PONTOISE', 'MALAKOFF', 'BOBIGNY')``. Nous avons ici quelque chose de beaucoup plus lisible et moins source d'erreur.

Mais comment dire que nous souhaiterions sélectionner toutes les communes de la région sauf ces quatre ci ? La solution SQL consiste à écrire la requête suivante : ``"NOM_COM" not in ('CERGY', 'PONTOISE', 'MALAKOFF', 'BOBIGNY')``. Le mot clef *not* permet de sélectionner ce qui n'est *pas* dans la liste. Mais une technique de Sioux consiste à sélectionner ces quatre communes comme dans l'exemple précédent puis d'*inverser* la sélection grâce à la fonction *Inverser la sélection* |icone-inverser| se trouvant dans la barre d'outils de la table attributaire.

Requête basée sur la géométrie
++++++++++++++++++++++++++++++++
Jusqu'ici nous avons vu des requêtes qui n'ont fait intervenir aucun concept géographique. Or l'avantage d'un SIG est justement que les entités gérées sont spatialisées et disposent donc d'attributs spatiaux. Pour une entité de type *polygone* nous pouvons donc avoir accès à sa superficie.

Ce fait est tout à fait intéressant lorsque nous souhaitons sélectionner des entités selon leurs tailles, même si aucun champ de superficie n'existe. Nous allons par exemple sélectionner les communes d'Île-de-France de plus de 20 km2. Nous retournons dans le menu de sélection par expression et en nous aidant du menu déroulant *Géométrie* qui se trouve dans le panneau central nous pouvons entrer directement la requête suivante ``$area > 20 * 1000000``. La fonction *$area* retourne la superficie de chaque entité exprimée dans les unités du SCR, à savoir ici des mètres. C'est pourquoi nous multiplions par *1000000* notre critère de taille. Souvent, pour ce type de requête nous sommes tentés de d'abord créer un vrai champ *Superficie* puis ensuite de requêter dessus, alors que ce n'est absolument pas nécessaire (:numref:`selection-area`).

.. figure:: figures/fen_select_area_volee.png
    :width: 25em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: selection-area
    
    Sélectionner sur la superficie sans champ de superficie.

Cet aspect peut bien sûr être couplé aux champs présents. Par exemple, il est tout à fait possible de sélectionner les communes dont la densité d'habitants est supérieure à 50 habitants par kilomètres carrés via la requête suivante : ``("POPULATION" / ( $area / 1000000)) > 50``. Il faut bien faire attention aux parenthèses et à la conversion de la superficie et la requête donne le résultat escompté. Cette requête remplace le fait de créer un champ *Superficie* puis un champ *Densité* puis une requête sur ce dernier champ.


..  _selection-attributaire-R:

Requêtes attributaires dans R
************************************
Version de R : 4.8.1

Une fois une couche vecteur chargée dans R, il est possible d'y effectuer tout type de requêtes attributaires. Faire ce type de requêtes dans R est un moyen puissant pour chaîner les opérations en évitant d'écrire en dur sur le disque tous les fichiers intermédiaires.

Dans cet exemple, nous effectuerons une requête sur une couche vecteur contenant les départements français stockée dans une variable nommée *departements* sous la forme d'un objet *sf*. Cette couche possèse un champ nommé *superficie* contenant les superficies de chaque département exprimé en km \ :sup:`2`. Dans l'exemple, nous sélectionnons tous les départements dont la superficie est supérieure à 1500 km  \ :sup:`2`. Nous sauvons le résultat de cette sélection dans une variable nommée *grands_dep*. Cette sélection n'est finalement rien d'autre qu'un *filtre* au sens de R, comme il est possible de les appliquer à tout objet de type *dataframe*.

.. code-block:: R

    grands_dep <- dep[(dep$superficie > 1500),]

.. note::
	Se référer à la documentation de base de R pour bien saisir la syntaxe employée pour `les filtres`_.


.. |icone-inverser| image:: figures/icone_inverser_selection.png
              :width: 20 px

.. |icone-deselect| image:: figures/icone_deselectionner.png
              :width: 25 px

.. |icone-select-expr| image:: figures/icone_selection_expression.png
              :width: 20 px

.. _les filtres: https://www.geeksforgeeks.org/how-to-filter-r-dataframe-by-values-in-a-column/
