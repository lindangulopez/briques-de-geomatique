..  _format-et-import-donnees:

Auteur : Paul Passy

Licence : |cc_by_nc_sa|

.. |cc_by_nc_sa| image:: figures/Cc-by-nc-sa_icone.png
              :width: 80 px

Données géospatiales, formats, import et export
=================================================

En géomatique, nous sommes amenés à travailler avec deux grands types données : les données rasters et les données vecteur. Chacun de ces types à des particularités et des domaines d'utilisation différents. La plupart des logiciels de géomatique sont capables de traiter ces deux types de données, même si certains sont plus spécialisés pour traiter l'un ou l'autre. Par exemple, le logiciel Orfeo ToolBox ne traite que du raster.

Il est possible dans certains cas de transformer une couche raster en une couche vecteur et réciproquement. Mais cette conversion ne se fait pas n'importe quand et est réservée à des objectifs précis.

Cette fiche va nous permettre de nous familiariser avec ces deux types de données de façon pratique ainsi qu'avec d'autres types de données pouvant avoir un caractère géospatial comme des données textes disposant de coordonnées, des traces GPS ou des photos géoréférencées.

.. contents:: Table des matières
    :local:

Les données rasters
---------------------

Les données rasters sont des données purement numériques organisées sous forme de tableau, aussi appelé *matrice*. Une couche raster est une couche rectangulaire (ou carrée) composée de pixels organisées en lignes et colonnes. Chaque pixel est caractérisé par une position au sein de cette grille, par une dimension et par une valeur. Les pixels sont souvent, mais pas toujours, de forme carrée et présentent tous la même forme. Chaque pixel est associée à une et une seule valeur. Cette valeur peut être totalement arbitraire, comme dans le cas d'un raster d'occupation du sol où les pixels en 1 seraient les pixels d'*eau*, les pixels en 2 serait des *forêts* etc ou bien ces valeurs peuvent avoir un réel sens physique. C'est notamment le cas des Modèles Numériques de Terrain (MNT) où chaque pixel présente une valeur d'altitude. C'est aussi le cas des images satellites où chaque pixel présente une valeur de radiance ou réflectance.

Dans l'exemple ci-après (:numref:`raster-principe`), nous avons un raster (grille gris clair), composée de pixels dont la dimension vaut *d*. Il s'agît d'un pixel à 10 colonnes et 7 lignes. Au centre, nous trouvons un pixel de valeur *x*. Par convention, les rasters sont repérés dans un plan dont l'origine est le pixel en haut à gauche. Ainsi, ce pixel *x* a comme coordonnées (7 ; -4). La définition de ce repère explique pourquoi dans les logiciels de géomatique, les pixels sont présentés comme ayant une dimension positive, la dimension en x, et une dimension négative, la dimension y.

.. figure:: figures/fig_raster_principe.png
    :width: 25em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: raster-principe
    
    Raster schématisé dans son repère cartésien.

Pour un aperçu concret de ces propriétés, nous pouvons charger la toute petite extraction d'un MNT nommée "*mnt_extract*". Cette petite dimension nous permet d'apprécier les pixels individuellement. Pour accéder aux informations du pixel, nous ouvrons ses propriétés via un clic droit sur cette couche dans le panneau des couches. Nous pouvons ainsi facilement comparer les informations et le raster en lui même (:numref:`raster-concret1`).

.. figure:: figures/fig_raster_concret_1.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: raster-concret1
    
    Dimensions x et y du raster.

Dans l'onglet *Information*, au niveau de *Dimensions* nous pouvons lire *X : 9* et *Y : 5*, soit 9 colonnes et 5 lignes (:numref:`raster-concret2`). Ces dimensions correspondent bien au nombre de pixels en colonnes (en vert) et en lignes (en rouge). L'*Origine* juste au-dessous correspond bien au centre du pixel en haut à gauche.

.. figure:: figures/fig_raster_concret_2.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: raster-concret2
    
    Dimensions x et y d'un pixel.

Pour la *Taille du Pixel*, nous constatons qu'un pixel mesure 22 m en *x* (en vert) et 30 m en *y* (en rouge). Nous constatons également que la dimension en *y* est bien négative.

.. note:: 
  **Valeurs manquantes** : il arrive fréquemment que certains pixels n'aient d'un raster n'aient pas de valeurs. Ces pixels sans valeurs sont souvent les pixels en bord de raster, mais pas seulement. Or un pixel ne peut pas être "vide", il doit obligatoirement posséder une valeur numérique. Une valeur est donc dédiée par convention à la valeur *no data*. Le problème est qu'il n'y a pas de règle fixe. C'est souvent une valeur comme -999 ou -9999 ou bien une autre valeur très négative. Dans QGIS, par défaut cette valeur est *-32767*.

Les formats rasters
***********************
	
Le principal format raster que nous rencontrons en géomatique est le format **GeoTIFF**, dont l'extension est *.tif* ou *.tiff*. Il s'agît d'une image *TIF*, comme il en existe dans le domaine de l'image numérique standard, mais qui inclut en plus un tag *cartographique*. Ce tag cartographique contient notamment les données relatives au système de coordonnées utilisé pour le fichier donné. Ce format est lu et écrit par tous les logiciels de géomatique. C'est le format utilisé pour stocker les modèles numériques de terrain ou les images Landsat par exemple.

Un autre format que nous pouvons rencontrer en télédétection est le format **JPEG 2000**, dont l'extension est *.jp2* ou *.jpg2*. Contrairement au GeoTIFF, il s'agît d'un format compressé (mais sans pertes). Une même image au format jpeg 2000 pèsera donc moins lourd qu'au format GeoTIIF à qualité identique. C'est donc un format que nous retrouvons souvent pour les images à très haute résolution, notamment les ortho-photos. L'IGN fournit par exemple ses ortho-photos dans ce format. Ce format peut être lu et écrit par tous les logiciels.

Les données vecteurs
----------------------

Le second type de données que nous utilisons en géomatique sont les données au format *vecteur*, aussi appelées *données vectorielles*. Contrairement aux rasters, il ne s'agît plus de matrices de pixels mais de formes élémentaires décrites par des équations. Heureusement pour nous, ces équations sont gérées par les logiciels et nous n'avons pas à nous en préoccuper. L'intérêt de cette description sous forme mathématique est que ces formes ne changent pas avec le niveau de zoom. Par exemple, lorsque nous zoomons sur un raster, un effet pixelisé apparaît. Alors qu'il est possible de zoomer à l'infini sur une entité vectorielle, nous ne verrons aucune altération.

L'autre particularité de ces données vecteurs est de pouvoir leur associer des données attributaires. À chaque entité, il est possible d'associer une infinité d'attributs. Par exemple, au polygone qui correspond à l'entité d'une commune, il est possible d'associer sa superficie, sa population, son code postal, son nom, sa proportion d'espaces verts ...

La géométrie des données vecteur
***********************************

Les données vecteur peuvent être de trois géométries fondamentales : points, lignes et polygones. 

Un **point** est un simple point décrit par un couple de coordonnées *X* et *Y*.

Une **ligne** est une succession de *nœuds* (aussi appelés *vertex*) reliés par des *arcs*. Chaque nœud possède une localisation *X* et *Y* et est associé à un arc (si le nœud se trouve au tout début ou à la toute fin de la ligne) ou à deux arcs (si le nœud se situe autre part sur la ligne).

Un **polygone** est également une succession de *nœuds* reliés par des *arcs*, mais dont la fin du dernier arc rejoint le début du premier arc. C'est donc une forme "fermée" à laquelle est associée une surface.

Chacune de ces trois géométries fondamentales peut se décliner dans sa forme *multi* : *multi-points*, *multi-lignes* et *multi-polygones* (:numref:`vecteur-principes`). Le principe est le même sauf que chaque entité peut être représentée par plusieurs formes. Par exemple, la Corse et la France sont deux formes différentes mais ne représentent qu'une seule entité étatique.

.. figure:: figures/fig_vecteur_principes.png
    :width: 30em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: vecteur-principes
    
    Les trois géométries fondamentales de vecteurs.

Dans la réalité, ces trois formes n'existent pas. Par exemple, un point est censé avoir une largeur et une longueur nulle. Or si c'était le cas, un point n'existerait pas. Ces formes sont des abstractions qui résultent de choix faits par l'utilisateur, en fonction de l'échelle de travail. Ainsi, la ville de Lyon sera un polygone si nous travaillons à l'échelle du Département du Rhône mais seulement un point si nous travaillons à l'échelle du monde. De même, une autoroute sera une surface sur une photo aérienne mais une ligne sur un atlas routier (:numref:`vecteur-echelles`).

.. figure:: figures/fig_vecteur_echelles.png
    :width: 40em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: vecteur-echelles
    
    L'Autoroute A15 vue comme une surface (A) ou comme une ligne (B).

La topologie des données vecteur
***********************************

Un aspect fondamental des données vecteur est leur *topologie*. C'est-à-dire les relations que les données ont entre elles et au sein d'elles mêmes. Deux entités peuvent être disjointes, superposées, juxtaposées... Ces relations doivent être respectées au sein d'une même couche géographique ou entre plusieurs couches différentes. Par exemple, la frontière entre la France et l'Allemagne est matérialisée par le cours du Rhin. Par conséquent, non seulement les polygones, issus d'une couche *Pays d'Europe*, *France* et *Allemagne* ne doivent pas se superposer ; mais la ligne *Cours d'eau du Rhin*, issue d'une autre couche, doit se placer très précisément sur la frontière entre les deux pays.

Cette définition de la topologie est fondamentale lorsque nous créons des données vecteur. 

Imaginons une couche vecteur des pays du monde. Dans l'immense majorité des cas, les polygones des pays sont juxtaposées les uns aux autres sans chevauchement. En effet, un territoire appartient à un et unique pays. Les cas de juxtaposition sont possibles dans le cas d'un territoire disputé par plusieurs pays. Ainsi, la Crimée peut être incluse dans les polygones *Ukraine* et *Russie*, tout comme Jérusalem-Est au sein des polygones *Israël* et *Palestine*. Le contraire d'un chevauchement est un *trou* entre deux polygones adjacents. Autant les chevauchements peuvent être possibles sur une couche des pays du monde, autant les creux n'existent pas. Tous les territoires, à l'exception de l'Antarctique, sont revendiqués par un pays.

.. note:: 
  **Pour briller en société** : un territoire non revendiqué est qualifié de `Terra nullius`_. Il n'existe qu'un seul territoire de ce type à la surface du globe, hors Antarctique. Il s'agît du territoire de `Bir Tawil`_. Lambeau de désert de quelques kilomètres carrés à la `frontière soudano-égyptienne`_ revendiqué ni par l'Égypte ni par le Soudan.

Les formats vecteurs
***********************

En géomatique, nous avons à faire à deux formats principaux : le format *shapefile* et le format *GeoPackage*.

Le format **shapefile**, d'extension *.shp*, est un format élaboré par la société ESRI, la société qui développe le logiciel ArcGIS. Bien qu'il ait été développé par une société commerciale, c'est un format ouvert. C'est le format vecteur le plus répandu, car utilisé depuis longtemps par énormément de monde. C'est un format déroutant de prime abord. En effet, un fichier *.shp* seul n'est pas utilisable. Une couche shapefile est en fait obligatoirement composée de plusieurs fichiers. Les fichiers associés ont les extensions suivantes :

* *.shx* : le fichier qui porte les informations relatives aux formes (obligatoire)
* *.dbf* : le fichier qui contient la table attributaire (obligatoire)
* *.prj* : le fichier qui contient le système de coordonnées (pas obligatoire mais très vivement recommandé)
* *.sbn* et *.sbx* : des fichiers d'index spatiaux (pas obligatoire mais souvent présent)

D'autres fichiers (jusqu'à 11 en tout !) peuvent accompagner un fichier shapefile. Ainsi, lorsque nous souhaitons copier-coller un shapefile d'un répertoire à un autre, il faut copier-coller tous les fichiers constitutifs en même temps. Là où certains multiplient les pains, ESRI multiplie les fichiers.

.. warning:: 
  Les données attributaires d'un shapefile sont stockées dans un fichier *.dbf* (*dBase database File*). Ce format date une peu et présente une limitation génante en SIG : les noms des champs sont limités à 10 caractères. Ainsi si vous souhaitez nommer un champ *population_20008*, ce champ sera renommé automatiquement en *population*, ce qui peut être source d'embarras par la suite. Il faut donc abréger les noms des champs.

Le format **GeoPackage** est un format ouvert, développé depuis 2014. Un GeoPackage peut être vu comme une base de données qui peut contenir plusieurs *tables*. Chaque table sera une couche vecteur indépendante. Ainsi, un seul fichier *.gpkg* correspondra à une ou plusieurs couches et sera manipulable plus facilement qu'une cohorte de fichiers shapefile. Depuis la version 3 de QGIS, c'est le format employé par défaut.

Nous pouvons par exemple imaginer un fichier *regions.gpkg* qui contiendrait simplement la couche des régions de France. Mais nous pouvons également avoir un fichier *limites_administratives.gpkg* qui contiendra une couche des communes, une des départements, une couche des régions, ... Notons qu'il n'y a pas de restrictions pour les noms des champs dans ce format, ils peuvent être aussi longs que souhaités. Il peut s'avérer pratique de regrouper plusieurs couches dans un même GeoPackage. Nous pouvons même inclure des couches rasters dans un fichier GeoPackage mais cela est peu utilisé.

.. warning:: 
  Il est tout à fait possible de mettre des espaces et des caractères spéciaux dans les attributs d'une couche vecteur, mais il est fortement recommandé de ne pas en mettre dans les noms de fichiers et les noms de champs. La présence de caractères spéciaux dans les chemins des fichiers et les noms de champs peuvent être sources d'erreur lors de traitements. 

Entre le format shapefile et le format GeoPackage, c'est à l'utilisateur de faire son choix selon son expérience. Personnellement je suis passé au format GeoPackage.

.. warning:: 
  Le GeoPackage souffre encore d'une certaine instabilité dans certains cas. Notamment, il y a un attribut obligatoire nommé *fid* qui correspond à un identifiant unique à chaque entité. Ce champ est géré par les logiciels sans intervention de l'utilisateur. Mais il se trouve qu'après certaines manipulations, comme des intersections ou des regroupements dans QGIS, ce champ ne soit plus unique, ce qui génère une erreur au moment de vouloir enregistrer son fichier. Le plus simple dans ce cas est de supprimer le champ *fid* et de laisser QGIS en reconstruire un automatiquement.

Travailler avec les GeoPackage dans QGIS
******************************************
Version de QGIS : 3.16.1

Concrètement, lorsque nous travaillons avec des GeoPackage, dans QGIS ou ailleurs, il y a une petite gymnastique à appréhender. Cette gymnastique peut être un peu déroutante au début lorsque nous avons l'habitude de travailler avec le format shapefile.

Un fichier GeoPackage n'est pas une couche géographique. C'est une base de données (simple) qui contient une ou plusieurs tables qui, elles, sont des couches géographiques. Nous allons voir en pratique cet aspect créant un GeoPackage nommé *hydro_seine.gpkg* dans lequel nous stockerons trois tables, i.e. trois couches géographiques, différentes :

* le réseau hydrographique du bassin (*hydro*)
* les bassins-versants unitaires associés à chaque tronçon de rivière (*hydro_bv*)
* les limites externes du bassin de la Seine (*limites_bv*)

Nous chargeons dans QGIS ces trois couches. Une fois ces couches chargées, nous allons pouvoir les exporter vers un une base GeoPackage. Pour cela, nous faisons un clic droit dans le panneau des couches sur la couche *hydro* (ou une autre des trois, sans importance), et nous sélectionnons le menu *Exporter* > *Sauvegarder les entités sous*. La fenêtre suivante s'ouvre (:numref:`export-gpkg`).

.. figure:: figures/fen_export_geopackage.png
    :width: 30em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: export-gpkg
    
    Création d'un GeoPackage.

Dans le champ *Format* nous spécifions que nous voulons exporter notre donnée en format *GeoPackage*. Dans le champ *Nom de fichier*, nous indiquons le chemin et le nom du GeoPackage que nous souhaitons créer. Dans le champ *Nom de la couche*, nous indiquons sous quel nom la couche géographique sera enregistrée, dans notre cas *hydro*. Danns le champ *SCR*, nous précisons dans quel SCR nous allons enregistrer notre couche. Nous cliquons enfin sur *OK*.

La nouvelle couche apparaît automatiquement dans le panneau des couches de QGIS. Nous remarquons qu'elle apparaît sous le nom suivant *hydro_seine hydro*, ce qui signifie qu'il s'agît de la couche (aussi appelée la *table*) *hydro* du GeoPackage nommé *hydro_seine*. Notons que si nous allons voir les propriétés de la couche, dans l'onglet *Information* nous trouvons la fenêtre suivante (:numref:`gpkg-prop`).

.. figure:: figures/fen_geopackage_proprietes.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: gpkg-prop
    
    Propriétés d'une couche contenue dans un GeoPackage.

Dans le champ *Nom*, nous trouvons le nom du fichier GeoPackage. Dans le champ *Chemin* nous trouvons son chemin d'enregistrement sur le disque. Et dans le champ *Source*, nous trouvons le chemin du GeoPackage assortie du nom de la couche *layername=hydro*.

Nous avons maintenant un fichier GeoPackage contenant une couche géographique. Nous allons à présent y ajouter nos deux autres couches (les bassins unitaires et les limites). Il suffit de faire un clic droit sur la seconde couche des bassins unitaires, de sélectionner *Exporter* > *Sauvegarder les entités sous...* La fenêtre suivante surgit (:numref:`export-gpkg-add`).

.. figure:: figures/fen_export_geopackage_add.png
    :width: 30em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: export-gpkg-add
    
    Ajout d'une table (i.e. d'une couche) dans un GeoPackage existant.

Dans le champ *Nom de fichier*, nous pointons vers le GeoPackage existant dans lequel nous souhaitons ajouter une couche. Dans le champ *Nom de la couche*, nous spécifions le nom de la couche que nous ajoutons, ici *hydro_bv*. Puis *OK*.

.. warning:: 
  Si jamais vous entrez un nom de couche déjà existant, une fenêtre vous demandant quoi faire surgira.
  
  .. figure:: figures/fen_geopackage_conflit.png
    :width: 25em
    :align: center
    :alt: alternate text
    :figclass: align-center
  
  Si il s'agit d'une inattention, nous cliquons sur *Annuler* et nous changeons le nom de la couche dans la fenêtre précédente. Sinon, nous pouvons *Écraser le fichier* ce qui écrasera complètement le GeoPackage ou *Écraser la couche* ce qui ne remplacera que la couche en question. Nous pouvons même *Ajouter à la couche*.

Nous pouvons maintenant répéter la manipulation pour la troisième et dernière couche, celle des *limites*. Nous avons maintenant un GeoPackage contenant trois couches. Lorsque nous ouvrons ce GeoPackage dans QGIS, par un glisser déposer par exemple, une fenêtre nous demandant quelle couche choisir apparaît (:numref:`gpkg-choix`).

.. figure:: figures/fen_geopackage_couche_choix.png
    :width: 30em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: gpkg-choix
    
    Choix de la couche à ouvrir.

Il est alors possible de tout sélectionner ou seulement un ou plusieurs couches. Si nous cliquons sur *Ajouter des couches à un groupe*. Les couches sont regroupées dans un groupe d'affichage dans le panneau des couches.

.. note:: 
  Il est tout à fait possible d'avoir des couches dans des SCR différents au sein d'un même GeoPackage. Il est également possible de stocker des couches rasters.

Il est également possible d'enregistrer le style d'une couche au sein d'un GeoPackage (:numref:`gpkg-style`). Il suffit de faire un clic droit sur la couche dont nous souhaitons conserver le style, de sélectionner le menu *Propriétés* et d'aller dans l'onglet *Symbologie*.

.. figure:: figures/fen_styles_enregistrer.png
    :width: 30em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: gpkg-style
    
    Enregistrement d'un style dans un fichier GeoPackage.

Dans le menu *Style*, nous sélectionnons *Enregistrer par défaut*. Et dans la fenêtre suivante, nous choisissons *Base de données source*. Maintenant, lorsque nous ouvrons cette couche dans QGIS, son style personnalisé est automatiquement appliqué.

Modifier les GeoPackage dans QGIS
************************************
Version de QGIS : 3.16.1

Il est tout à fait possible de renommer ou de supprimer une couche au sein d'un fichier GeoPackage. Il faut néanmoins passer par un chemin qui ne se devine pas forcément de prime abord. Dans l'interface prncipal de QGIS, il faut aller dans le menu *Base de données* et sélectionner *Gestionnaire BD...* (:numref:`menu-bdd`).

.. figure:: figures/fig_bdd.png
    :width: 20em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: menu-bdd
    
    Ouverture du gestionnaire de base de données.

Le gestionnaire de base de données intégrés à QGIS s'ouvre. C'est à partir de ce gestionnaire que nous pourrons explorer et modifier la structure de nos GeoPackage (:numref:`gestionnaire-bdd`).

.. figure:: figures/fen_gestionnaire_bdd.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: gestionnaire-bdd
    
    Le gestionnaire de base de données de QGIS.

En premier lieu, il est nécessaire de *connecter* notre base de données GeoPackage. Pour cela, nous faisons un clic droit sur le menu *GeoPackage* et nous sélectionnons *Nouvelle connexion...* Nous pointons alors vers le GeoPackage que nous souhaitons gérer, ici *hydro_seine.gpkg*. Ce GeoPackage apparaît maintenant dans la liste des GeoPackages connectés (:numref:`gpkg-connectes`).

.. figure:: figures/fig_gpkg_connectes.png
    :width: 15em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: gpkg-connectes
    
    Liste des GeoPackages connectés.

Nous pouvons "dérouler" notre GeoPackage afin de voir les couches (les tables) qu'il contient (:numref:`gpkg-couches-liste`).

.. figure:: figures/fig_gpkg_couches_liste.png
    :width: 15em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: gpkg-couches-liste
    
    Liste des tables d'un GeoPackage.

Nous retrouvons bien nos trois tables, plus éventuellement une table de style si nous avons sauvé un style par défaut. Dans le panneau principal de ce gestionnaire de bases de données, nous trouvons trois onglets *Info*, *Table* et *Aperçu*, dont les noms résument bien ce qu'on y trouve (:numref:`table-bdd`). 

.. figure:: figures/fen_gpkg_bdd_table.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: table-bdd
    
    La table associée à une couche d'un GeoPackage.

Nous y retrouvons la table attributaire de la couche plus une colonne de géométrie *geom*. C'est normal, dans l'esprit *Base de données*, la géométrie est un attribut comme un autre.

En faisant un clic droit sur une couche dans ce gestionnaire, il est possible de *Renommer* ou *Effacer* la couche. Une fois l'intervention effectuée il peut être nécessaire de rafraîchir les connexions avec l'outil *Actualiser* |icone-actualiser| pour voir les changements apparaître.

.. note:: 
  Si par ailleurs la couche est également chargée dans QGIS, ces modifications ne sont pas visibles immédiatement. Il peut être nécessaire de fermer puis de rouvrir le GeoPackage modifié pour voir les modifications.

Dans ce gestionnaire de base de données, il est également possible de construire ses requêtes SQL, mais c'est un autre chapitre.


..  _importer-des-donnees-raster:

Importer des données rasters
-----------------------------

Pour rappel, les rasters sont des données matricielles géoréférencées. Il s'agît d'une image composée de pixels disposés en lignes et colonnes. Chaque pixel présente une valeur numérique unique dont le sens dépend de la nature du raster. Chaque pixel dispose également d'un couple de coordonnées géographiques X et Y.

Importer des données rasters dans un logiciel ou un script est une tâche fondamentale. L'importation est plus ou moins naturelle selon les outils. Nous allons voir ici comment importer de telles données rasters dans différents logiciels.

..  _import-raster-qgis:

Importer un raster dans QGIS
*****************************
Version de QGIS : 3.16.1

Il existe deux façons d'importer un raster dans QGIS. La plus simple est de faire un simple glisser-déposer depuis l'explorateur de fichier vers le panneau d'affichage de QGIS. La seconde est de passer par le menu *Couche* > *Ajouter une couche* > *Ajouter une couche raster...*

Dans cet exemple nous allons importer un raster de modèle numérique de terrain (MNT) issu de la mission SRTM et localisé sur une portion des Alpes françaises. Une fois le raster chargé, il apparaît dans le panneau d'affichage de QGIS (:numref:`raster-dans-qgis`).

.. figure:: figures/fen_raster_dans_qgis.png
    :width: 45em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: raster-dans-qgis
    
    Chargement d'un raster dans QGIS.

Nous remarquons que le nom du raster apparaît dans le panneau des couches sur la gauche. Si plusieurs couches sont chargées, seule la couche au dessus de la pile est visible.

.. warning::

	Les valeurs qui apparaissent sous le nom du raster dans le panneau des couches correspondent simplement aux valeurs associées à la symbologie employée. Il ne s'agît nullement des valeurs minimales et maximales du raster.

Il est ensuite possible de déplacer le raster ou de zoomer et dézoomer via la barre d'outils *Navigation cartographique* |icone-zoom-dezoom|. La dernière icône *Zoom sur l'emprise totale* permet de zoomer sur l'ensemble des couches chargées. Cette fonctionnalité s'avère lorsque nous sommes perdus dans l'affichage. Notons que nous pouvons également zoomer simplement sur l'étendue d'une couche donnée en faisant un clic droit sur son nom dans le menu des couches et en sélectionnant *Zoomer sur la couche*.

Améliorer les contrastes d'un raster
++++++++++++++++++++++++++++++++++++++

Dans QGIS, il existe un outil pratique pour améliorer les contrastes d'un raster. La barre d'outils correspondante n'est pas visible par défaut. Pour l'afficher dans l'interface, il suffit d'aller dans le menu *Préférences* > *Barres d'outils* et de cocher la case *Raster*. Une fois la barre d'outils activée, elle apparaît avec les autres barres d'outils en haut de l'interface |icone-raster-contraste|.

Finalement, seule la première icône de cette barre d'outils est la plus utile, celle intitulée *Histogramme cumulatif de l'emprise locale utilisant l'emprise actuelle*. Cette fonction est très intense lorsque nous zoomons sur une zone du raster et que nous souhaitons optimiser les contrastes sur cette zone (:numref:`zoom-contraste`). 

.. figure:: figures/fig_zoom_contraste.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: zoom-contraste
    
    A : zoom sans optimisation des contrastes ; B: même zoom mais avec optimisation des contrastes via l'outil de la barre d'outils raster.

Interroger les valeurs des pixels d'un raster
++++++++++++++++++++++++++++++++++++++++++++++

Il est possible d'accéder aux valeurs des pixels d'un raster chargé. Cela se fait facilement à l'aide de l'outil *Identifier des entités* |icone-identifier|.

Une fois l'outil sélectionné, le curseur de la souris est assorti d'un *i* et nous pouvons cliquer sur un pixel donné. Lors du clic, la valeur du pixel sous-jacent s'affiche dans le panneau *Résultats de l'indentification* à droite. Nous disposons même, dans le menu *(Dérivé)* des coordonnées du clic (:numref:`identifier-pixel`).

.. figure:: figures/fig_identifier_pixel.png
    :width: 20em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: identifier-pixel
    
    Interrogation d'un pixel.

.. tip::

	Il est possible de copier la valeur dans le presse papier simplement en faisant un clic droit sur cette valeur dans ce panneau de résultats.

Exploration des propriétés d'un raster
++++++++++++++++++++++++++++++++++++++++

Il est souvent utile de recueillir quelques données relatives à la couche proprement dite. Pour cela, il suffit de faire un clic droit sur la couche dans le panneau des couches et de sélectionner *Propriétés*. La fenêtre de propriétés apparaît alors (:numref:`raster-proprietes`).

.. figure:: figures/fen_raster_proprietes_qgis.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: raster-proprietes
    
    Propriétés d'une couche raster.

Un onglet intéressant est celui nommé *Information*. Nous y trouvons les informations suivantes :

* **Nom** : simplement le nom de la couche

* **Chemin** : le chemin du raster sur le disque dur

* **SCR** : le système de projection du raster. Nous y retrouvons le code EPSG et l'intitulé exact de la projection

* **Emprise** : l'emprise de la couche exprimée dans les unités de la projection

* **Unité** : l'unité de la projection, souvent *mètres* ou *degrés*

* **Type de données** : façon dont le raster est encodé. Ici, il s'agît d'un nombre entier signé de seize bits

* **Bande 1** : quelques statistiques descriptives du raster comme le maximum, la moyenne et le minimum.

* **Taille du pixel** : la taille du pixel exprimée dans les mêmes unités que le système de projection. Par convention, la seconde dimension est toujours négative.

.. tip::

	Une autre information intéressante se trouve plus bas dans ce même onglet d'Information. Dans la section *Bandes* nous trouvons la valeur numérique utilisée comme *no data*.

L'autre onglet intéressant à ce niveau est l'onglet *Histogramme*. Nous y trouvons l'histogramme des valeurs des pixels du raster considéré (:numref:`histo-raster`).

.. figure:: figures/fen_histo_raster_qgis.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: histo-raster
    
    Histogramme du raster.

.. note::

	La qualité graphique de l'histogramme ainsi que sa lisibilité ne sont pas merveilleuses.


..  _import-raster-R:

Importer un raster dans R
*****************************
Version de R : 4.1.1

Il est tout à fait possible d'importer des données rasters dans R afin de les traiter en utilisant ce langage de programmation. Il est simplement nécessaire d'utiliser une librairie (un *package*) dédiée. Le package historiquement le plus employé est le `package raster`_. Depuis quelques années, un package qui se veut plus performant, mais moins employé, a été développé. Il s'agît du `package stars`_. Ces deux packages s'installent facilement, soit via l'interface graphique de RStudio par exemple, soit pas la commande R dédiée, rappelée ci-après.

.. code-block:: R

   install.packages("stars")
   install.packages("raster")

.. tip::

	Cette `page présente une comparaison`_ des fonctionnalités offertes par ces deux packages dédiés au traitement raster.

..  _import-raster-R-stars:

Avec stars
++++++++++++

La première chose à faire est d'appeler le package ``stars``. Une fois cet appel effectué, la commande à utiliser est ``read_stars(raster_a_importer)`` :

.. code-block:: R

   library(stars)
   
   b3 <- read_stars("Data/LC08_L2SP_037034_20201027_20201106_02_T1_SR_B3.TIF")

Dans cet exemple nous importons une bande spectrale Landsat dans une variable *b3* qui contiendra ce raster au format *stars*. Il est ensuite possible de manipuler ce raster dans R.

Il est également possible de charger avec Stars des :ref:`rasters-multi-bandes`, construits préalablement. La syntaxe est tout à fait identique, comme nous le voyons ci-dessous où nous chargeons un raster multi-bandes contenant les bandes 2 à 5 d'une image Landsat 8 (*stack_L8_2-5.tif*) prise au-dessus des Bouches-du-Rhône.

.. code-block:: R

   library(stars)
   
   rast_mult <- read_stars("stack_L8_2-5.tif")

Une fois un raster chargé, il est possible de regarder quelques informations basiques comme le système de coordonnées de référence (SCR), la résolution spatiale en X et en Y et le nombre de bandes. Ces trois informations s'obtiennent via les commandes suivantes.

.. code-block:: R

   library(stars)
   
   # affichage du CRS
   st_crs(rast_mult)
   # le code EPSG est sur la dernière ligne de la sortie
   
   # affichage de la résolution en X et Y
   st_dimensions(rast_mult)
   # les dimensions correspondent aux "delta"
   
   # affichage du nombre de bandes
   dim(rast_mult)[3]

.. warning::
	Dans le cas de l'affichage du nombre de bandes, il faut bien vérifier que le *3* correspond bien à la dimension des bandes. Pour le savoir, à la sortie de *st_dimensions()*, la dimension des bandes se nomme *band*.


Affichage de rasters dans R
++++++++++++++++++++++++++++

Comme pour un graphique, il est possible d'afficher une vue du raster avec R. Il suffit d'employer la commande ``plot(mon_raster)``.

.. code-block:: R

   plot(b6)

Dans cet exemple, nous affichons le raster stocké dans la variable *b6* (:numref:`R-plot-raster`). Par défaut, les contrastes sont améliorés au maximum.

.. figure:: figures/fig_R_plot_raster.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: R-plot-raster
    
    Affichage d'un raster avec R.

Dans le cas d'un raster multi-bandes, l'appel à cette fonction ``plot`` permet d'afficher toutes les bandes en une seule fois.

.. code-block:: R

   plot(rast_mult, axes = TRUE)

Nous ajoutons ``axes = TRUE`` afin de tracer les axes gradués dans le système de coordonnées de l'image (:numref:`R-plot-multi-bandes`).

.. figure:: figures/fig_R_plot_multi-bandes.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: R-plot-multi-bandes
    
    Affichage d'un raster multi-bandes avec R.

.. warning::
	Les numéros des bandes qui apparaissent sur le graphique ne sont que l'ordre dans lequel ces bandes sont stockées. C'est à l'utilisateur de savoir à quelle bande Landsat correspond chaque bande du raster. Ainsi, ici la bande 1 du raster est en fait la bande *Bleu* (donc la *2*) de notre image Landsat 8.

Nom des rasters dans R
++++++++++++++++++++++++++++

Une fois un raster chargé dans R avec le package *stars*, ce raster porte un nom propre indépendant du nom de la variable qui le stocke.Par exemple, sur la figure (:numref:`R-plot-raster`) nous avons tracé le raster stocké dans la variable *b6* mais ce raster en lui-même se nomme *LC08_L2SP_037034_20201027_20201106_02_T1_SR_B3.TIF*, comme nous le constatons sur le titre de la figure. Il est tout à fait possible de renommer ce raster. Par exemple si nous souhaitons simplement le nommer *bande6*, nous utiliserons la méthode ``names()`` du package *stars*. La commande pour le renommer est alors très simple.

.. code-block:: R

   names(b6) <- 'bande6'

Nous indiquons simplement à ``names`` la variable qui contient le raster à renommer et nous lui attribuons un nouveau nom.


..  _import-raster-Python:

Importer un raster dans Python
********************************
Version de Python : 3.8.10

Il est tout à fait possible d'importer des rasters dans un script Python. Il existe différentes librairies permettant de le faire. Nous allons ici nous focaliser sur la librairie *rasterio*. Il est tout d'abord nécessaire d'installer cette librairie. Ensuite, elle s'utilise de façon très simple. Elle s'importe dans le script via le mot clé *import*. L'idée est de commencer par importer un fichier raster puis ensuite d'accéder à une bande se trouvant dans ce fichier. La bande ainsi récupérée sera stockée sous forme d'un objet *array* (Numpy), c'est-à-dire un tableau de chiffres. La commande ci-dessous montre la procédure dans le cas d'un raster mono bande.

.. code-block:: Python

   # chargement de la librairie de gestion des rasters rasterio
   import rasterio

   # le chemin vers le raster mono-bande à charger et chargement
   rast = rasterio.open('./Landsat_13/LC08_L2SP_196030_20190613_20200828_02_T1_SR_B5.TIF')
   # on récupère ensuite la bande qui nous intéresse (ici qu'une bande)
   band = rast.read(1)

Ici nous avons importé un raster mono-bande que nous avons stocké dans une variable nommée *rast*. Puis nous avons chargé la première (et unique) bande de ce raster dans une variable nommée *band*.

Il est également possible d'importer un raster multi-bandes, la procédure est tout à fait identique.

.. code-block:: Python

   # chargement de la librairie de gestion des rasters rasterio
   import rasterio

   # le chemin vers le raster multi-bandes à charger
   rast_multi = rasterio.open('./Landsat_13/stack_L8_2-5.tif')
   # on récupère ensuite la bande qui nous intéresse, la 2ème par exemple
   band_2 = rast_multi.read(2)

Nous avons ici importé un raster multi-bandes, stocké dans une variable *rast_multi* puis nous avons chargé la deuxième bande de ce raster dans la variable *band_2*.

Une fois ces rasters importés, il est possible d'accéder à différentes caractéristiques associées comme les dimensions, le SCR ou les coordonnées de l'emprise.

.. code-block:: Python

   # afficher le SCR d'un raster
   rast.crs
   # la largeur du raster
   rast.width
   # la hauteur du raster
   rast.height
   # les coordonnées de l'emprise
   rast.bounds

Pour accéder à la résolution spatiale du raster, i.e. la taille des pixels, il y a une petite manipulation à connaître. La taille d'un pixel est indiquée dans les paramètres de *transformation* du raster. Ces paramètres sont une sorte de tableau et la résolution en *X* est la 1ère entrée du tableau et la résolution en *Y* est la 4ème entrée. Il est important de noter que la résolution en *Y* est donnée par convention en négatif, il faut donc la multiplier par *-1*.

.. code-block:: Python

   # récupération des paramètres de transformation
   gt = rast.transform

   # dimension en X (1er paramètre des transformations)
   dimX = gt[0]
   # dimension en Y (4ème paramètre des transformations)
   dimY = -gt[4]

Lorsque nous disposons d'un raster multi-bandes, il est possible de récupérer le nombre de bandes et d'accéder à une bande en particulier.

.. code-block:: Python

   # le chemin vers le raster multi-bandes à charger
   rast_multi = rasterio.open('./Landsat_13/stack_L8_2-5.tif')

   # affichage du nombre de bandes
   rast_multi.count

   # accès à une bande spécifique d'un raster multi-bandes, la 4 par exemple
   b4 = rast_multi.read(4)

Affichage de rasters dans Python
++++++++++++++++++++++++++++++++++

Une fois un raster chargé, il est possible de l'afficher. Cette affichage passe par le package Python *matplotlib* très couramment utilisé dans tout ce qui touche à la visualisation. Il est ensuite possible de régler quelques paramètres d'affichage comme le contraste, le titre, la taille, ...

.. code-block:: Python

   #import de matplotlib
   from matplotlib import pyplot
   
   # le chemin vers le raster multi-bandes à charger
   rast_multi = rasterio.open('./Landsat_13/stack_L8_2-5.tif')

   # définition d'un objet figure qui ne contiendra qu'une figure "ax" dont les dimensions seront de 8 par 8
   fig, ax = pyplot.subplots(1, figsize=(8, 8))
   # on affiche la bande 4 du raster multi-bandes
   # selon une palette de gris "gray"
   # à l'emplacement de la figure "ax"
   # avec un titre personnalisé "title"
   # et en améliorant les contrastes avec vmin et vmax, le noir est attribué à 0 et le blanc à 0.5
   show((rast_multi, 4), cmap='gray', ax=ax, title='Bande 4 Landsat 8', vmin=0, vmax=0.5)

Nous obtenons la vue suivante (:numref:`plot-bande-python`). Nous constatons que les axes correspondent bien au référentiel géographique du raster affiché. Il est possible de modifier le contraste en jouant sur les paramètres *vmin* et *vmax*.

.. figure:: figures/fig_plot_bande_python.png
    :width: 30em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: plot-bande-python
		
    Affichage d'un raster dans Python en teintes de gris contrasté.

Il est également possible d'afficher plusieurs bandes côte à côte avec des teintes de couleurs différentes. Ici, par exemple, nous affichons les bandes des domaines du Bleu, Vert et Proche Infrarouge dans des dégradés de bleu, vert et rouge.

.. code-block:: Python

   #import de matplotlib
   from matplotlib import pyplot
   
   # le chemin vers le raster multi-bandes à charger
   rast_multi = rasterio.open('./Landsat_13/stack_L8_2-5.tif')
   
   # définition d'un objet figure qui contiendra 3 graphiques ax1, ax2 et ax3
   # paramétrage de la taille avec figsize
   fig, (ax1, ax2, ax3) = pyplot.subplots(1,3, figsize=(21,7))
   # on trace la bande Bleu en dégradé de bleu, avec un titre et amélioration des contrastes
   show((rast_multi, 1), ax=ax1, cmap='Blues', title='Bande Bleu', vmin=0, vmax=0.1)
   # on trace la bande Verte en dégradé de vert, avec un titre et amélioration des contrastes
   show((rast_multi, 2), ax=ax2, cmap='Greens', title='Bande Verte', vmin=0, vmax=0.15)
   # on trace la bande PIR en dégradé de rouge, avec un titre et amélioration des contrastes
   show((rast_multi, 1), ax=ax3, cmap='Reds', title='Bande PIR', vmin=0, vmax=0.15)

Nous obtenons la figure suivante (:numref:`plot-3bandes-python`).

.. figure:: figures/fig_plot_3_bandes_python.png
    :width: 50em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: plot-3bandes-python
		
    Affichage de trois bandes en différents dégradés.


..  _importer-des-donnees-vecteur:

Importer des données vecteurs
----------------------------------

Pour rappel, les données vecteur (ou vectorielles) sont les données de base en SIG. Ce sont des formes géométriques de type ponctuelle, linéaire ou surfacique auxquelles sont associées des données attributaires. Charger ce type de données dans un logiciel ou un script est une brique fondamentale de la géomatique. Nous allons voir ici comment importer de telles données vecteurs dans différents logiciels.

..  _import-donnees-vecteur-qgis:

Importer une couche vecteur dans QGIS
*************************************
Version de QGIS : 3.16.1

Comme pour l'import de données raster, il existe deux façons d'importer des données vectorielles dans QGIS. La plus simple est de faire un *glisser déposer* de son fichier depuis l'explorateur de fichiers vers le panneau central de QGIS.

La seconde façon est de passer par le menu :menuselection:`Couche --> Ajouter une couche --> Ajouter une couche vecteur...`.

.. note::

	Depuis quelques années, le format vecteur utilisé par défaut dans QGIS est le format *geopackage*, dont l'extension est *.gpkg*. Il a pour vocation de concurrence le format *shapefile*.

Dans cet exemple, nous allons importer la couche des États du monde (*ne_10m_admin_0_countries*) disponible sur le site `Natural Earth`_. Cette couche contient les polygones de tous les pays du monde. Chaque polygone représente un pays, et à chacun de ces polygones différentes données attributaires sont associées comme le nom, la population, le PNB ... (:numref:`vecteur-dans-qgis`).

.. figure:: figures/fen_vecteur_dans_qgis.png
    :width: 40em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: vecteur-dans-qgis
		
    Le vecteur des pays du monde chargé dans QGIS.

Polygones et données attributaires
++++++++++++++++++++++++++++++++++++

Les polygones, ici les pays, sont les objets spatiaux. À chacun de ces objets spatiaux sont associées des données attributaires. Ces données sont stockées dans une *table attributaire*. Il s’agit d'un tableau de données structuré de façon rigoureuse. Il est possible d'accéder à cette table attributaire en faisant un clic droit sur le nom de la couche dans le panneau des couches et en sélectionnant ``Ouvrir la table d'attributs``. La table s'ouvre alors dans une nouvelle fenêtre (:numref:`table-attributs-qgis`).

.. figure:: figures/fen_table_attributs_qgis.png
    :width: 40em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: table-attributs-qgis

    La table attributaire associée à notre couche.

Comme tout tableau, nous y trouvons des lignes et des colonnes. Chaque ligne correspond à un *enregistrement* ou à un *individu*, dans notre cas chaque ligne correspond donc à un pays. Chaque colonne correspond à un *attribut*. Par exemple, l'attribut *Name* correspond au nom du pays en anglais, l'attribut *Pop_est* correspond à la population estimée etc... Nous remarquons qu'un champ peut, pour certains pays, être non renseigné. Sa valeur sera alors *NULL*.

.. warning::

	**De l'importance des métadonées** : normalement, à chaque donnée diffusée devraient être associées des métadonnées, même succinctes. Une métadonnée est une information présentant la donnée diffusée. Ici, par exemple, sans métadonnées, impossible de bien saisir le sens de tous les attributs de cette couche. Malheureusement, cette métadonnée est très souvent absente...

Cette table attributaire est liée aux objets spatiaux que sont les polygones affichés dans QGIS. Ainsi si nous sélectionnons la première ligne (*South Africa* dans le cas de cet exemple), cette ligne se surligne en bleu. Si nous retournons dans le panneau d'affichage de QGIS, nous constatons que l'objet géographique correspondant à cette ligne est également sélectionnée. Le polygone de l'Afrique du Sud apparaît en jaune, ce qui signifie que nous avons bien sélectionné l'Afrique du Sud (:numref:`selection-table-objet`). 

.. figure:: figures/fig_selection_table_objet.png
    :width: 40em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: selection-table-objet

    Lien entre table d'attributs et objets spatiaux.

Notons que ce lien fonctionne bien sûr dans l'autre sens. Si nous sélectionnons un pays sur la couche via l'outil ``Sélectionner les entités avec un rectangle ou un simple clic`` |icone_selection-clic|, la ligne correspondante dans la table sera également sélectionnée.

.. tip::

	Pour désélectionner des entités, soit nous cliquons sur "rien" soit nous cliquons sur l'icône ``Désélectionner toutes les entités`` |icone_deselect|.


Interroger une couche vecteur
+++++++++++++++++++++++++++++++

Il est possible d'interroger rapidement une couche vecteur dans QGIS. Il suffit de se servir de l'outil ``Identifier des entités`` en cliquant sur l'icône |icone-identifier|. En cliquant sur un objet géographique avec cet outil, il n'est pas sélectionné même s'il apparaît surligné en rouge. Les attributs associés à cette entité s'affichent dans le panneau ``Résultats de l'identification``. Les attributs sont listés dans le même ordre que dans la table attributaire (:numref:`identifier-vect`).

.. figure:: figures/fig_identifier_vect_qgis.png
    :width: 25em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: identifier-vect

    Identification des attributs d'une entité.

Dans ce panneau, il est possible de copier dans le presse-papier une valeur d'attribut. Un simple clic droit sur la valeur à copier, suivi de la sélection du menu ``Copier la valeur d'attribut`` suffit. Nous pouvons également copier toutes les valeurs d'attributs en sélectionnant ``Copier les attributs des entités``. Il est alors possible de coller ces valeurs sous forme de tableau dans un tableur. Il est même possible de copier l'entité avec sa géométrie en sélectionnant ``Copier l'entité``. L'entité est ainsi exportée au format texte *wkt* (*Well Known Text*).

Nous pouvons également accéder à des propriétés intrinsèques de l'entité comme la superficie ou son périmètre en déroulant le menu *Dérivé*.

Explorer les propriétés d'une couche vecteur
++++++++++++++++++++++++++++++++++++++++++++++

Il est souvent utile d'explorer les propriétés d'une couche vecteur chargée dans QGIS. Il suffit de faire un clic droit sur le nom de la couche dans le panneau des couches et de sélectionner le menu ``Propriétés``. La fenêtre des propriétés s'affiche (:numref:`vect-proprietes`).

.. figure:: figures/fen_vecteur_proprietes_qgis.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: vect-proprietes

    Propriétés d'une couche vecteur.

Dans l'onglet ``Information``, nous trouvons les renseignements suivants :

* **Nom** : simplement le nom de la couche

* **Chemin** : le chemin de la couche sur le disque dur

* **Stockage** : le format dans lequel est enregistré la couche. Pour une couche vecteur ce sera la plupart du temps *ESRI Shapefile* ou *Geopackage*

* **Encodage** : l'encodage de la table attributaire. La plupart des données sont maintenant encodées en UTF-8 qui permet d'afficher tous les caractères, même les plus exotiques.

* **Géométrie** : le type de géométrie de la couche. Ici, nous avons *Polygon (MultiPolygon)*. Ça signifie que nous avons des entités sous forme de polygones mais qui peuvent être composées de plusieurs polygones. Par exemple, la France continentale et la Corse sont représentées par deux polygones différents mais ils ne forment qu'une seule entité. Une seule ligne de la table attributaire est associée à cette entité multi polygones.

* **SCR** : le système de projection de la couche, ici c'est la projection globale *WGS 84*

* **Emprise** : les coordonnées extrêmes de la couche

* **Unité** : les unités du système de projection de la couche. Ici, il s'agît de *degrés*. D'autres projections utilisent le *mètre*.

* **Décompte d'entités** * : le nombre d'entités dans la couche, ici 177. Ce chiffre correspond également au nombre de lignes dans la table attributaire. Selon cette couche, il y a 177 pays.

À ce niveau, l'autre onglet intéressant est l'onglet *Champs*. Nous y trouvons la façon dont les différents champs sont sont formatés. Nous pouvons voir quels champs sont de type *texte*, *entiers* ou *booléens*... (:numref:`champs-types`).

.. figure:: figures/fen_champs_types_qgis.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: champs-types

    Le format des champs de la table attributaire.

Ici, nous constatons que certains champs sont codés en *Integer* (*entiers*) et d'autres en *String* (pas le sous-vêtement mais le *texte*...).

.. note::

	Le format des champs n'est pas toujours aussi évident qu'il n'en a l'air. Par exemple, nous pourrions penser qu'un champ stockant les numéros des départements français serait de type *entier*. Pourtant ce champ devrait être de type *texte*. En effet, les départements corses sont *2A* et *2B*. De plus les neuf premiers départements seraient identifiés en *1*, *2*, *3* ... au lieu de *01*, *02*, *03*, ...

..  _import-donnees-vecteur-R:

Importer une couche vecteur dans R
*************************************
Version de R : 4.8.1

Il est possible d'importer des couches vecteurs dans R afin de les manipuler via ce langage de programmation. Il existe plusieurs librairies (i.e. *package*) qui permettent cet import. Nous verrons ici l'utilisation du package ``sf``. Une documentation détaillée de ce package est disponible sur la `page web dédiée à sf`_. La première chose à faire est d'installer ce package via l'interface graphique de RStudio ou via la ligne de commande :

.. code-block:: R

   install.packages("sf")

Nous prendrons l'exemple de l'import d'une couche vecteur au format geopackage contenant les départements de France métropolitaine, nommée *departements_france_L93.gpkg*. La commande d'import se nomme ``st_read(mon_vecteur)``. Ici, nous stockerons cette couche vecteur dans un objet sf nommé *dep*.

.. code-block:: R

   dep <- st_read("departements_france_L93.gpkg")

Il est ensuite possible de l'afficher sous forme d'un graphique à l'aide de la commande ``plot(mon_vecteur)``.

.. code-block:: R

   plot(dep)

Par défaut, pour les objets *sf*, autant de figures sont tracées qu'il n'y a de champs attributaires. Dans cet exemple, notre couche vecteur contient 6 champs attributaires, nous obtenons donc 6 figures (:numref:`plot-vecteur-R`).

.. figure:: figures/fig_plot_vecteurs_R.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: plot-vecteur-R

    Affichage d'une couche vecteur avec R.

.. note::

	Le package *sf* est capable d'importer de très nombreux formats vecteurs : *gpkg*, *shp*, *geojson*, ...

Une fois une couche vecteur importée, il est possible d'afficher différentes informations de base relatives à la couche. Le plus simple est de simplement entrer le nom de la couche pour faire apparaître dans la sortie de la console de R différentes informations (:numref:`vecteur-R-basique`).

.. figure:: figures/fig_R_sortie_basique_vecteur.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: vecteur-R-basique

    Affichage d'informations de base d'une couche vecteur dans R.

Nous y trouvons le type de géométrie (*Multipolygon* ici), le CRS et un aperçu des 10 premières lignes de la table attributaire. Pour avoir des informations précises à propos du système de coordonnées de référence de la couche, la commande *st_crs()* est suffisante.

.. code-block:: R

   library(sf)
   # chargement de la couche
   dep <- st_read("departements_france_L93.gpkg")
   # affichage des informations sur le SCR
   st_crs(dep)

La commande prend simplement en argument le nom de la variable contenant la couche vecteur. Le code EPSG se trouve tout en bas de la sortie dans la console.

Importer une couche vecteur dans PostGIS 
***************************************************

Nous allons voir ici comment importer une couche vecteur dans une base de données PostGreSQL/PoqtGIS.

En utilisant QGIS
++++++++++++++++++++++
Version de QGIS : 3.20.0

L'ajout de données dans une base PostGIS peut se faire via QGIS. Dans QGIS, dans le panneau ``Explorateur``, nous faisons un clic droit sur la ligne ``PostGIS`` et nous sélectionnons ``Nouvelle connexion``. La fenêtre de configuration de la connexion s'affiche. Dans le champ ``Nom``, nous nommons la connexion. Ce nom est libre, nous mettons par exemple *geodata_my_user*. Dans le champ ``Hôte``, nous mettons *localhost* pour spécifier que notre base est en local et non sur un serveur distant. Dans ``Base de données`` nous entrons le nom de la base à laquelle nous souhaitons nous connecter, à savoir *geodata*. Dans le panneau ``Authentification``, nous devons entrer les identifiants de l'utilisateur de la base, à savoir *my_user* et son mot de passe tel que défini dans DBeaver au moment de la création de cet utilisateur. Il est possible de se souvenir de ces identifiants en cliquant sur :guilabel:`Stocker`. Ensuite nous cliquons sur :guilabel:`OK` (:numref:`qgis-connexion-base`).

.. figure:: figures/fen_connexion_base_qgis.png
    :width: 400px
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: qgis-connexion-base
    
    Connexion à notre base dans QGIS.

Une fois la connexion réalisée, elle apparaît dans le menu ``PostGIS`` du panneau ``Explorateur``. La base apparaît sous le nom défini lors de la connexion *geodata_my_user*. Cette base contient un schéma *public* (schéma de base créé automatiquement) qui est vide (:numref:`qgis-base-connectee`).

.. figure:: figures/fig_qgis_base_connectee.png
    :width: 200px
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: qgis-base-connectee
    
    La base connectée à QGIS disposant d'un seul schéma encore vide.

Il s'agît maintenant de charger une couche dans cette base. Nous allons d'abord crer un nouveau schéma afin de ne pas travailler dans le schéma *public* (pour ranger un peu nos données). Ici, nous allons charger la couche des limites des terres émergées telles que fournies par le site *Natural Earth* (`Land`_). Nous allons donc créer un schéma nommé *natural_earth*. Pour cela, nous faisons un clic droit sur notre base dans le panneau ``Explorateur`` et nous choisissons ``Nouveau schéma...`` Dans la fenêtre suivante, nous nommons notre schéma *natural_earth*. Ce nouveau schéma apparaît bien à côté du schéma *public*.

Pour y ajouter la couche souhaitée, nous allons dans le menu :menuselection:`Base de données --> DB Manager...` Dans la fenêtre qui s'ouvre, nous déplions le menu ``PostGIS`` et la base *geodata_my_user* puis nous sélectionnons le schéma dans lequel nous souhaitons ajouter une couche *natural_earth* (:numref:`qgis-add-table`).

.. figure:: figures/fen_qgis_add_table_to_base.png
    :width: 450px
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: qgis-add-table
    
    Ajout d'une couche à un schéma donné.

Nous cliquons ensuite sur :guilabel:`Import de couche/fichier` en haut de la fenêtre. La fenêtre d'import s'ouvre (:numref:`qgis-select-table`).

.. figure:: figures/fen_qgis_select_table.png
    :width: 350px
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: qgis-select-table
    
    Sélection de la couche à importer.

Dans le champ ``Source`` nous pointons vers la couche à importer qui peut être de n'importe quel format. Dans les champs ``Schéma`` et ``Table``, nous vérifions le schéma d'import et nous spécifions un nom pour la table correspondante à cette couche qui sera créée dans la base. Nous pouvons également sélectionner ``Créer un index spatial``. Cet index permettra d'accélérer les traitements sur cette couche. C'est toujours une bonne idée d'en avoir un. Puis nous cliquons sur :guilabel:`OK`. La nouvelle couche apparaît bien sous le schéma correspondant de la table *geodata_my_user* dans le panneau ``Explorateur``. Il est possible d'afficher la couche en double cliquant dessus.


..  _importer-des-donnees-texte:

Importer des données textes
--------------------------------

Il peut arriver de devoir traiter une donnée géographique simplement stockée dans un fichier texte de type *csv*. Il peut s'agir de données capturées avec un GPS sur le terrain, ou de tout autre type de données récupérées ici ou là. Dans la plupart des cas, il s'agit de données ponctuelles. C'est de ce cas là dont nous traiterons ici.

.. note::

	Les géométries linéaires ou surfaciques peuvent tout à fait être stockées au format texte, mais dans ce cas il s'agira d'un format spécifique connu sous le nom de *Well Known Geometry* dont l'extension est *.wkt*.

Concrètement, les fichiers *csv* qui peuvent être importés doivent respecter quelques règles simples. Chaque ligne doit correspondre à une entité. En fait, le fichier *csv* en lui même peut être considéré comme une table attributaire. Parmi les colonnes, doivent obligatoirement figurer une colonne stockant les coordonnées en *X* et une autre stockant les coordonnées en *Y*. Le système de coordonnées de référence de ces coordonnées doit absolument être connu à priori par l'utilisateur.

.. tip::

	Lorsque vous recevez un tel fichier, pensez toujours à demander au fournisseur quel est le système de coordonnées utilisé. À fortiori, si c'est vous qui diffusez un tel fichier, accompagnez le toujours de la mention du système de coordonnées utilisé.

.. contents:: Table des matières
    :local:

Importer une couche texte dans QGIS
*************************************
Version de QGIS : 3.16.1

Dans cet exemple, nous allons importer dans QGIS l'implantation de stations de mesures de la qualité de l'eau dans des cours d'eau Corse ("*stations_qualite_Corse_L93.csv*"). Premièrement il est possible d'ouvrir ce fichier dans un tableur afin de se rendre compte de sa structure (:numref:`fichier-csv`).

.. figure:: figures/fig_fichier_csv.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: fichier-csv
    
    Structure du fichier csv à importer.

.. warning::

	Les tableurs comme LibreOffice Calc ou Excel ne gèrent pas forcément de façon optimale les types de champs. Ici, par exemple, le champ *CdStationM* est en fait de type *texte*. En observant la deuxième ligne, nous nous apercevons que LibreOffice Calc a considéré que ce champ était de type numérique. Alors qu'en réalité cette station a pour identifiant "06215990". Non seulement le type n'est pas respecté mais en plus le "0" initial a été supprimé. Il peut être plus prudent d'ouvrir ce csv avec un éditeur de texte de type bloc-note, même si la lecture s'en trouve compliqué.

Dans ce fichier, la première ligne correspond à l'en-tête des colonnes (leurs noms). En anglais, l'en-tête est appelé *Header*. Ce fichier correspondra en fait à la table attributaire de la couche géographique que nous allons créer. Chaque ligne correspond à une entité géographique, à savoir, ici, une station de mesure. Chaque colonne est un attribut d'une station. Parmi ces attributs, deux sont indispensables : les coordonnées X et Y. Nous les retrouvons dans les champs *coord_X* et *ccord_Y*. Il s'agît de coordonnées exprimées en Lambert 93.

Le principe va être de charger ce fichier dans QGIS en lui disant de se baser sur ces deux champs pour définir la géométrie de la couche.

L'import se fait via le menu *Couche* > *Ajouter une couche* > *Ajouter une couche de texte délimité*. La fenêtre suivante s'affiche (:numref:`import-csv`).

.. figure:: figures/fen_import_csv_qgis.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: import-csv
    
    Importation d'une couche de texte délimité dans QGIS.

Dans le champ *Nom de fichier* nous sélectionnons le fichier *csv* à importer, ici "*stations_qualite_Corse_L93.csv*". Le champ *Nom de la couche* est personnalisable et le champ *Encodage* permet de définir l'encodage de la table d'attributs. *UTF-8* est la plupart du temps la meilleure solution.

Dans le panneau *Format de Fichier*, il est possible de spécifier quel est le séparateur de champ utilisé. Par exemple, un "vrai" fichier *csv* sépare les champs en utilisant une virgule (comma en anglais). Mais ici, notre fichier a un séparateur en *Point-virgule*.

Dans le panneau *Option des champs et enregistrements*, il est possible de définir si il y a ou non un en-tête, si il faut ignorer des lignes au début du document, si le séparateur de décimal est une virgule... Souvent les options par défaut dans ce panneau sont suffisantes.

Dans le panneau *Définition de la géométrie*, nous définissons le type de géométrie (point, ligne, polygone) et les champs qui la renferment. Ici, le *champ X* est "coord_X" et le *champ Y* est "coord_Y". Il est encore nécessaire de définir le système de coordonnée utilisé, le Lambert 93 dans notre cas (EPSG 2154).

Le panneau *Échantillon de données* nous permet de vérifier que le tableau est bien lu avant de la charger dans QGIS.

Une fois ces réglages effectués, il n'y a plus qu'à cliquer sur *Ajouter*. La nouvelle couche de points apparaît bien dans le panneau central. Après l'import d'un tel fichier, il est toujours bien de vérifier la localisation de notre couche en ajoutant au projet une donnée de référence comme un fond OpenStreetMap ou autre. Se reporter à la fiche correspondante si besoin : :ref:`importer-des-sdonnees-web`.

.. figure:: figures/fig_import_csv_osm.png
    :width: 15em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: import-csv-check
    
    Vérification de l'import avec une couche externe (OSM).

À ce stade, il faut bien noter que la couche est temporaire, elle est simplement affichée dans QGIS mais n'est pas sauvée sur le disque. Pour l'exporter en une couche géographique, nous devons faire un clic droit sur le nom de la couche dans le panneau des couches et sélectionner le menu *Exporter* > *Sauvegarder les entités sous...* et ensuite définir un chemin d'export.

.. warning::

	Nous ne pouvons qu'insister sur le rôle crucial des métadonnées en général et dans ce cas là en particulier. Le fichier en lui même n'est pas suffisant, il faut connaître également le système de cordonnées correspondant aux coordonnées X et Y. Il est également important de préciser ce que représentent les autres champs.


..  _importer-tableur:

Importer un tableur
----------------------

Dans certains cas il peut être utile d'importer un tableur ne disposant d'aucune information géographique dans un projet SIG. Par exemple si nous disposons d'une couche géographique des pays du monde, nous pourrions avoir besoin de lui adjoindre des informations relatives à leurs populations récupérées par ailleurs sous forme d'un tableur. Souvent l'idée sous-jacente est de faire une jointure attributaire entre une couche géographique et le tableur importé.

Importer un tableur dans QGIS
*********************************
Version de QGIS : 3.16.1

Dans QGIS, rien de plus simple, il suffit de faire un glisser déposer du tableur depuis l'explorateur vers le panneau central de QGIS. Le fichier est chargé, il apparaît dans le panneau des couches, et est traitée comme une table attributaire indépendante. QGIS reconnaît aussi bien le format *ods* de LibreOffice que *xls* ou *xlsx* de Microsoft Office. 


..  _importer-des-points-GPS:

Importer des points GPS
----------------------------

Après avoir capturé une trace GPS d'un itinéraire, soit à l'aide de son smartphone soit à l'aide d'un récepteur GPS dédié, il peut être intéressant d'importer cette trace dans un logiciel SIG. Cette trace est enregistrée au format *.gpx*. Cette importation nous permettra de comparer notre trace avec des données tierces et de la manipuler comme n'importe quelle donnée géoréférencée.

La plupart des logiciels SIG offrent une fonction d'import de ce type de fichiers *.gpx*.

Importer des points GPS dans QGIS
*************************************
Version de QGIS : 3.16.1

La façon la plus simple d'importer un fichier *.gpx* dans QGIS est de faire un simple glisser-déposer du fichier depuis l'explorateur de fichiers vers le panneau principal de QGIS. Dans cette exemple, nous allons charger le fichier nommé *trace_GPS_Malakoff.gpx* qui est une trace GPS capturée sur la commune de Malakoff dans les Hauts-de-Seine (92).

Un fichier *.gpx* se comporte un peu comme un fichier *geopackage*. Il s'agît d'un fichier contenant plusieurs couches, ce qui nous est indiqué au moment de l'import (:numref:`import-gpx`).

.. figure:: figures/fen_gpx_import_qgis.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: import-gpx

    Import d'un fichier gpx dans QGIS.

Ce fichier *gpx* contient 5 couches :

* **route_points** : couche de type points contenant les points à suivre tels que définis par l'utilisateur
* **route** : couche de type polylignes contenant les arcs reliant les points de la couche précédente
* **track_points** : couche de type points contenant les points (i.e. les nœuds) de la trace GPS capturée
* **tracks** : couche de type polylignes contenant les arcs reliant les points de la couche *track_points*
* **waypoints** : couche de type points contenant les éventuels points d'intérêts, comme les prises de notes audio ou vidéos, relevés par l'utilisateur

Dans notre cas, aucune *route* n'avais été définie, nous pouvons donc ignorer ces deux couches en ne sélectionnant que les 3 dernières. Nous pouvons sélectionner l'option *Ajouter des couches à un groupe*, ce qui permettra de charger ces trois couches dans un groupe dans le panneau des couches. Le chargement se fait après avoir cliqué sur *OK* (:numref:`gpx-dans-qgis`).

.. figure:: figures/fig_gpx_dans_qgis.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: gpx-dans-qgis

    Les couches gpx chargées dans QGIS et dans un groupe de couches.

De façon basique, les couches de points apparaissent comme des couches vecteurs ponctuelles et les couches de polylignes comme des couches vecteurs linéaires. Nous remarquons que nous avons bien groupé ces éléments de la trace dans un groupe qui a pris le nom du fichier initial *trace_GPS_Malakoff*.

.. warning::
	Notons que le système de coordonnées de référence d'une couche *gpx* est la plupart du temps de type WGS 84 (4326).

Comme pour n'importe quelle couche vecteur, nous pouvons consulter les tables attributaires associées à ces données. Nous commençons par ouvrir celle de la couche *waypoints* (:numref:`gpx-table-waypoints`).

.. figure:: figures/fen_gpx_table_waypoints.png
    :width: 30em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: gpx-table-waypoints

    La table attributaire de la couche waypoints.

La plupart des colonnes ne sont pas renseignées. Mais nous trouvons des informations intéressantes dans les colonnes *time* et *name*. Dans cette dernière nous trouvons les noms de fichiers associés à ces points. Il s'agît ici de fichiers *.jpg* donc de photos. Il s'agît de deux photos prises par l'utilisateur pendant l'enregistrement de la trace GPS. Dans la colonne *time*, nous trouvons la date et l'heure de prises de ces photos.

.. note::
	La colonne *name* contient le nom du fichier lors de la prise de vue. C'est un nom donné automatiquement par l'application. Même si l'utilisateur renomme la photo dans l'application, le nom ne sera pas mis à jour dans ce champ (ce qui aurait été pratique).

Nous pouvons regarder la table associée à la couche *track_points* (:numref:`gpx-table-track-points`).

.. figure:: figures/fen_gpx_table_track_points.png
    :width: 30em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: gpx-table-track-points

    La table attributaire de la couche track_points.

Dans notre cas, seules trois colonnes sont intéressantes. La première est *track_srg_id* qui est un simple identifiant numérique unique de chaque point. La deuxième est *ele* qui correspond à l'altitude du point lors de son enregistrement. La troisième est *speed* qui contient la vitesse à laquelle se déplaçait l'utilisateur lors de l'enregistrement du point.

.. note::
	L'altitude associée à chaque point est celle mesurée par le capteur GPS employé par l'utilisateur. Cette valeur est donc plus ou moins précise en fonction du matériel, du nombre de satellites captés à ce moment, des conditions atmosphériques ... Il en est de même pour la vitesse.

Enfin, la couche *tracks*, dans notre cas, ne contient aucune information intéressante.


..  _importer-photos-localisees:

Importer des photos localisées
----------------------------------

Lors d'une excursion sur son terrain d'études, ou simplement suite à une randonnée, il peut être intéressant d'importer dans un projet de géomatique des photos précisément localisées. Ces photos permettront par exemple de compléter notre analyse avec des prises de vue "réelles" correspondant à la réalité sur le terrain.

Ces photos doivent bien sûr disposer de coordonnées géographiques (de type GPS) dans leurs métadonnées. Les métadonnées d'un fichier photo *.jpg* sont incluses dans la partie *Exif* du fichier. *Exif* est l'acronyme de *Exchangeable image file format*. Concrètement, c'est une partie du fichier image qui est utilisée pour stocker différentes métadonnées relatives à la prise de vue : nom, date, heure, distance focale, vitesse d'obturation, coordonnées géographiques, ... Toutes ces métadonnées ne sont pas forcément renseignées. L'utilisateur peut choisir de les renseigner ou pas au moment de la prise de vue. Ici, nous considérons que nous avons pris nos photos de terrain au travers de l'application *OsmAnd* afin d'être sûr que nos prises de vue incluent la localisation.

Ces photos peuvent par exemple servir à confronter une image satellite à la réalité terrain. Elles peuvent également être utiles pour construire ou valider une classification d'usage du sol à partir de ces images de télédétection.

Cette manipulation peut se faire avec différents outils faciles d'utilisation.

Importer des photos localisées dans QGIS
*****************************************
Version de QGIS : 3.16.1

Dans cette section, nous allons voir comment importer des photos géoréférencées dans QGIS. Il existe différentes façons d'importer des photos localisées dans QGIS. Ils existent plusieurs extensions dédiées à cette tâche. Mais nous allons voir comment le faire en utilisant les capacités internes à QGIS sans passer par une extension.

Pour l'exemple, nous allons superposer nos photos localisées à une trace GPS que nous avons capturée en même temps.

Import des localisations
+++++++++++++++++++++++++++
La première étape consiste à importer les localisations des photos et de les stocker dans une couche de points. Au préalable il est nécessaire de stocker les photos à importer au sein d'un répertoire dédié sur son disque. Dans cet exemple, notre répertoire se nomme *tagged_photos*. Cela se fait simplement en passant par la *Boîte à outils de traitements* > *Création de vecteurs* > *Importer des photos géolocalisées*. Le menu suivant s'ouvre (:numref:`import-photos`).

.. figure:: figures/fen_import_photos_qgis.png
    :width: 30em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: import-photos

    Importer les localisations des photos dans QGIS.

Concrètement, ce module va lire les métadonnées de localisation depuis les *Exif* de chaque photo et les transformer en une couche de points. Dans le champ *Dossier source* nous indiquons le répertoire contenant les photos à importer, ici *tagged_photos*. Dans le champ *Photos* nous indiquons le chemin et le nom de la couche de points qui sera créée, ici *tagged_photos_georef.gpkg*. La couche résultante apparaît automatiquement après avoir cliqué sur *Exécuter*.

La couche de points apparaît bien dans le panneau des couches. Nous pouvons explorer sa table d'attributs. Nous n'avons qu'une photo dans notre cas (:numref:`photos-table`).

.. figure:: figures/fen_photos_table.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: photos-table

    La table attributaire associée à la localisation des photos.

Dans cette table nous retrouvons les champs suivants :

* **photo** : le chemin de la photo sur le disque
* **filename** : le nom de la photo
* **directory** : le chemin du répertoire contenant la photo
* **altitude** : l'altitude prise de vue
* **direction** : l'azimut dans lequel pointait l'appareil photo au moment de la prise de vue (non renseigné ici)
* **longitude** : la longitude de la localisation de la photo (en WGS 84)
* **latitude** : la latitude de la localisation de la photo (en WGS 84)
* **timestamp** : la date et l'heure de la prise de vue

Affichage des photos sous forme de popups
++++++++++++++++++++++++++++++++++++++++++++
Une fois les localisations importées, il est utile d'afficher directement les photos dans notre projet. Cet affichage se règle dans les *Propriétés* de la couche de points de localisations, puis dans l'onglet *Affichage* (:numref:`aff-photos`).

.. figure:: figures/fen_affichage_photos_qgis.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: aff-photos

    Réglage de l'affichage des photos dans QGIS.

Nous allons utiliser les possibilités *html* de l'affichage de QGIS. Dans le champ *Afficher le nom*, nous sélectionnons le champ contenant le chemin vers la photo à afficher, à savoir *filename*. Ensuite nous réglons le code HTML à utiliser dans le panneau *Infobulle HTML*. Nous y entrons :

.. code-block:: html

   <img src="file:///[% photo %]" width="350" height="250">

Nous reconnaissons la balise *img* signifiant en HTML que nous allons afficher une image. *src* indique la source de l'image à afficher. La source commence par *file://* car l'image est stockée sur notre disque et non en ligne. La partie *[% photo %]* sera remplacée à la volée par le chemin correspondant au champ *filename*. Et enfin, suivent les dimensions d'affichage de la photo.

.. tip::
   Pour les connaisseurs du langage HTML, il est possible de paramétrer encore plus finement toute cette partie.

Nous cliquons ensuite sur *OK*. Il n'y a plus qu'à activer l'affichage des infobulles dans l'interface de QGIS. Il suffit de cliquer sur l'icône *Afficher les infobulles* |icone_infobulle| dans la barre d'outils générale. La photo s'affichera dès que nous survolerons le point correspondant avec le curseur de la souris (:numref:`photos-popup`). Notons, qu'il peut y avoir un petit temps de latence entre le survol et l'affichage.

.. figure:: figures/fig_photo_popup.png
    :width: 25em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: photos-popup

    Apparition du popup de la photo au survol du curseur.

Affichage des miniatures des photos au niveau des points
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Pour un aperçu rapide des photos, il est possible de changer la symbologie de la couche des points de localisation en y mettant une miniature de la photo correspondante. Ce réglage se fait dans la *symbologie* de la couche. Nous faisons un clic droit sur la couche dans le panneau des couches, nous sélectionnons *Propriétés* puis nous allons dans l'onglet *Symbologie* (:numref:`photos-miniature`).

.. figure:: figures/fig_photo_miniature.png
    :width: 40em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: photos-miniature

    Affichage de la miniature des photos comme symbologie.

Nous réglons la symbologie en *Symbole Unique*. Dans le panneau juste au-dessous, nous sélectionnons la ligne qui est par défaut à *Symbole simple* et dans *Type de symbole*, nous choisissons *Symbole image raster*. De nouvelles options apparaissent dans le bas de la fenêtre. À côté du champ vide, nous cliquons sur l'icône |icone_options|. Un menu apparaît dans lequel nous cliquons sur *Type de champ:string* puis nous sélectionnons *photo (string)*. Ici, nous lui disons de lire le chemin qui se trouve dans le champ *photo* de la table attributaire. Nous pouvons ensuite régler la taille de la miniature dans le champ *Largeur*. Puis nous cliquons sur *OK* (:numref:`photos-miniature-aff`).

.. figure:: figures/fig_photo_minitaure_affichage.png
    :width: 30em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: photos-miniature-aff

    La miniature de la photo utilisée comme symbologie.

Par contre, la taille de la miniature ne change pas avec le niveau de zoom, ce qui peut rendre l'affichage général du projet plus ou moins illisible.

.. note::
   Cette présentation de cet outil est inspirée de la documentation fournie sur ce `blog`_.


..  _importer-des-sdonnees-web:

Importer des données Web
----------------------------

Dans certains cas il peut être utile d'ajouter "en fond" des données tierces comme les fonds de cartes Open Street Map, Google Earth ou les otrho-photos de l'IGN. Ces fonds de carte seront chargées "à la volée" depuis Internet et ne seront pas téléchargés en local.

Données Web dans QGIS
****************************
Version de QGIS : 3.16.1

Il existe deux façons d'ajouter des données tierces au sein d'un projet QGIS. Soit via une extension, soit via l'explorateur incorporé à l'interface. Dans les deux cas une connexion à Internet est requise.

Via l'extension QuickMapServices
++++++++++++++++++++++++++++++++++

La solution la plus facile est de passer par une extension nommée *QuickMapServices*. Au préalable, il est donc nécessaire d'installer cette extension.

Une fois cette extension installée, elle apparaît dans le menu *Internet* qui se trouve dans la barre des menus principaux, en haut de l'interface. Certains fonds de cartes sont directement accessibles via ce menu dans *Internet* > *QuickMapServices*. Parmi les données proposées, la plus couramment utilisée est le fond *OSM* (OpenStreetMap) (:numref:`osm-dans-qgis`).

.. figure:: figures/fig_osm_dans_qgis.png
    :width: 45em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: osm-dans-qgis
    
    Zoom sur un quartier de Singapour depuis OSM dans QGIS.

.. tip::

	Il est préférable d'ajouter au préalable dans QGIS une couche géographique de notre zone d'étude, puis d'ajouter un fond de carte externe. De cette manière, le fond de carte chargé sera automatiquement centré sur notre zone d'intérêt. Sinon, le monde entier est chargé, ce qui peut s'avérer un peu long.

Il est possible d'accéder à quelques propriétés des couches distantes chargées. Nous y accédons par un clic droit sur le nom de la couche dans le panneau des couches puis *Propriétés*. La fenêtre des propriétés apparaît alors.

.. figure:: figures/fen_osm_proprietes_qgis.png
    :width: 30em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: osm-prop
    
    Propriétés de la couche OpenStreetMap.

Parmi les informations intéressantes nous trouvons :
 * **URL** : l'URL source de la couche
 
 * **SCR** : le système de coordonnées de la couche. Ici, il s'agît d'un SCR global de type Mercator qui a la particularité d'être projeté et d'être exprimé en mètres et non en degrés.
 
 * **Unité** : l'unité du système du SCR, ici le *mètre*

QGIS est capable de changer le SCR à la volée de ces fonds de cartes externes pour respecter le SCR des données de l'utilisateur (:numref:`google-earth_l93`).

.. figure:: figures/fig_google_earth_L93.png
    :width: 45em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: google-earth_l93
    
    Le fond Google Earth projeté en Lambert 93.

Il est tout à fait possible d'ajouter des données autres que celles proposées par défaut comme les images Google Earth ou les ortho-photos de l'IGN. Il suffit de retourner dans le menu *QuickMapServices* et de sélectionner *Search QMS*. Un panneau nommé *Search QMS* apparaît en bas à droite de l'interface (:numref:`search-qms`). 

.. figure:: figures/fig_search_qms.png
    :width: 20em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: search-qms
    
    Rechercher d'autres données avec QuickMapServices.

Nous entrons dans le champ du haut un mot clé de la couche que nous souhaitons ajouter. Par exemple *Google satellite* pour ajouter les images Google Earth. Une autre donnée intéressante est la couche des ortho-photos de l'IGN. Pour les obtenir, nous pouvons entrer *ortho IGN* dans la barre de recherche et d'ajouter la couche *BDOrtho IGN* (:numref:`ortho-ign`).

.. figure:: figures/fig_ortho_ign_qgis.png
    :width: 45em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: ortho-ign
    
    L'ortho-photo de l'IGN au niveau du bâtiment Olympe de Gouges de l'Université de Paris (Paris 13ème).

.. warning::

	Il peut être tentant d'utiliser les images Google Earth pour créer des polygones d'entraînement pour classifier une image satellite. Mais il faut être très prudent pour ce genre de démarche. Nous n'avons aucune idée de la date de prise de vue de l'image Google. La région peut avoir changé. La saison est également importante, un champ nu sur l'image Google Earth sera peut-être une belle prairie sur votre image satellite à classer.

Via le panneau des couches
++++++++++++++++++++++++++++

Si l'explorateur n'apparaît pas automatiquement, nous le réactivons en allons dans le menu *Préférences* > *Panneaux* > *Explorateur (2)*. Un explorateur s'ajoute alors sur la partie gauche de l'interface (:numref:`explorateur-qgis`).

.. figure:: figures/fig_explorateur_qgis.png
    :width: 20em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: explorateur-qgis
    
    L'explorateur intégré à QGIS.

Dans cet explorateur nous allons ajouter des données tierces grâce au menu *XYZ tiles*. Afin d'utiliser ce menu, il est tout d'abord nécessaire de connaître l'URL de la couche à ajouter. Trouver cet URL peut être laborieux. Ici, nous allons ajouter une entrée vers les images Google Satellite. Le plus simple pour récupérer son URL est de passer par QuickMapServices, puis de regarder dans les propriétés de la couche tel que que nous l'avons vu dans la section précédente. Nous copions l'URL se trouvant dans le champ *URL*.

Ensuite, nous faisons un clic droit sur le menu *XYZ tiles* et nous sélectionnons *Nouvelle connexion...* La fenêtre suivante apparaît (:numref:`xyz-qgis`).

.. figure:: figures/fen_connexion_xyz_qgis.png
    :width: 30em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: xyz-qgis
    
    Ajout d'une nouvelle *connexion XYZ*.

Dans le champ *Nom*, nous pouvons indiquer *Google Satellite* (ou tout autre nom, ça n'a pas d'importance). Dans le champ *URL* nous collons l'URL copiée précédemment. Les autres réglages peuvent être laissés par défaut dans notre cas. Nous cliquons sur *OK* et la couche est ajoutée. Une nouvelle entrée correspondante apparaît dans le menu *XYZ tiles* (:numref:`xyz-new-layer`).

.. figure:: figures/fig_xyz_new_layer_qgis.png
    :width: 15em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: xyz-new-layer
    
    La nouvelle couche *XYZ* apparaît dans le menu.

Au final, cette solution peut être utilisée comme une sorte de raccourci qui nous évite de passer par la fonction de recherche de l'extension *QuickMapServices*.


..  _exporter-des-donnees-rasters:

Exporter des données rasters
----------------------------------

Lorsqu'un traitement raster a été effectué, il est peut être utile d'enregistrer la couche résultante dans un format d'échange facile comme le *GeoTiff*. Cette manipulation est surtout utile pour exporter des couches rasters créées au travers de scripts R ou Python.

..  _export-donnees-raster-R:

Exporter un raster depuis R
*************************************
Version de R : 4.8.1

Au cours d'un traitement avec un script R, différents rasters peuvent avoir été créés. Dans certains cas, il est bien de pouvoir les exporter au format *GeoTiff* par exemple, afin de les rendre exploitables au travers d'un logiciel comme QGIS ou d'un autre langage de programmation comme Python. Chaque package de gestion des rasters (``raster`` ou ``stars`` notamment) propose sa propre méthode d'export.

Export depuis stars
+++++++++++++++++++++

Le package ``stars`` propose une méthode nommée *write_stars*. Cette ligne de commande est simple, elle prend en paramètre le nom du raster à exporter tel qu'il est connu dans le script R, et un chemin de destination de l'export. Ici, nous allons exporter un raster stocké dans une variable *bande* vers un fichier raster au format *GeoTiff* nommé *export_bande.tif*.

.. code-block:: R

   write_stars(bande, dsn='mon_chemin/export_bande.tif')

Une fois cette commande exécutée, un nouveau fichier *.tif* contenant notre raster apparaît bien dans notre explorateur de fichier.

.. note::
    Si l'objet *Stars* est multi-bandes, le geotiff exporté sera lui aussi multi-bandes.


..  _export-raster-Python:

Exporter un raster depuis Python
*********************************
Version de Python : 3.8.10

Une fois des traitements rasters réalisés au sein d'un script Python, il est souvent utile d'exporter le résultat dans un fichier raster standard de type GeoTiff. Cette exportation peut se faire à l'aide du package *rasterio*. L'idée générale est qu'à la fin de notre traitement nous disposons d'un *array* Numpy qui correspond à une bande raster. Et c'est cette bande raster pour l'instant stockée sous forme de *array* que nous allons exporter en GeoTiff. Le principe va être de créer un raster vide que nous allons définir selon des caractéristiques de base (hauteur, largeur, CRS, type, ...), puis nous allons écrire la bande à exporter dans ce raster vide. Dans l'exemple suivant, nous allons importer une bande raster depuis un fichier *tif* existant et nous allons exporter cette bande dans un nouveau raster. Cette manipulation n'a évidemment aucun intérêt à part montrer la procédure d'export.

.. code-block:: Python

   # chargement de la librairie de gestion des rasters rasterio
   import rasterio

   # le chemin vers le raster mono-bande à charger et chargement
   rast = rasterio.open('./Landsat_13/LC08_L2SP_196030_20190613_20200828_02_T1_SR_B5.TIF')
   # on récupère ensuite la bande qui nous intéresse sous forme d'un array Numpy
   band = rast.read(1)
   
   # exportation de cette bande
   # on commence par créer un raster "vide" dont on définit les paramètres de base
   raster_export = rasterio.open(
        './Landsat_13/band.tif', # on définit le chemin d'export
        'w', # on indique qu'on va écrire quelque chose dans ce raster vide
        driver='GTiff', # on précise qu'on va exporter un GeoTiff
        height=band.shape[0], # le raster sera de même hauteur que le nombre de lignes dans la bande
        width=band.shape[1], # le raster sera de même largeur que le nombre de colonnes dans la bande
        count=1, # le raster n'aura qu'une seule bande
        dtype=band.dtype, # le type du raster sera le même que celui des nombres contenus dans les pixels de la bande
        crs=rast.crs, # on définit le CRS comme étant le même que celui du raster initialement chargé
        transform=rast.transform, # on applique les mêmes paramètres de transformation que ceux du raster initialement chargé
   )
   # enfin on écrit dans ce raster la bande à exporter
   raster_export.write(band, 1)

Nous obtenons bien un raster *band.tif* qui est identique au raster initialement chargé.


..  _exporter-des-donnees-vecteurs:

Exporter des données vecteurs
----------------------------------

Lorsqu'un traitement vecteur a été effectué, il est peut être utile d'enregistrer la couche résultante dans un format d'échange facile comme le *Geopackage*. Cette manipulation est surtout utile pour exporter des couches vecteurs créées au travers de scripts R ou Python.

..  _export-donnees-vecteur-R:

Exporter un vecteur depuis R
*************************************
Version de R : 4.8.1

Lorsque nous avons effectué des traitements vecteurs dans R sur un objet de type *sf*, il peut être utile de l'exporter dans un format intelligible par d'autres logiciels et langages de programmation comme les formats *.gpkg*, *.shp*, *geojson*, ... Ici, nous exporterons un vecteur stocké sous forme d'un objet *sf* et se trouvant dans une variable nommée *dep*. Nous l'exporterons dans un geopackage nommé *limites_administratives* et nous enregistrerons la couche sous le nom *departements*. La commande *sf* à utiliser est ``st_write()`` qui prend en paramètre, le nom de l'objet *sf* à exporter, un chemin vers la destination de stockage, et le driver à utiliser (à définir selon le format d'export choisi).

.. code-block:: R

   st_write(dep, dsn = "limites_administratives.gpkg", layer=departements, driver="GPKG")





.. |icone-actualiser| image:: figures/icone_actualiser.png
              :width: 20 px

.. |icone-zoom-dezoom| image:: figures/icone_zoom_dezoom.png
              :width: 140 px

.. |icone-raster-contraste| image:: figures/icone_raster_contraste.png
              :width: 280 px

.. |icone-identifier| image:: figures/icone_identifier.png
              :width: 25 px

.. |icone_selection-clic| image:: figures/icone_selection_clic.png
              :width: 25 px

.. |icone_deselect| image:: figures/icone_deselect_vecteur.png
              :width: 25 px

.. |icone_infobulle| image:: figures/icone_infobulle.png
              :width: 20 px

.. |icone_options| image:: figures/icone_options.png
              :width: 25 px

.. _blog: https://opengislab.com/blog/2020/8/23/mapping-and-viewing-geotagged-photos-in-qgis
.. _Land: https://www.naturalearthdata.com/downloads/110m-physical-vectors/
.. _Terra nullius: https://fr.wikipedia.org/wiki/Terra_nullius
.. _Bir Tawil: https://fr.wikipedia.org/wiki/Bir_Tawil
.. _frontière soudano-égyptienne: https://www.openstreetmap.org/search?query=bir%20tawil#map=11/21.8742/33.6353
.. _Natural Earth: https://www.naturalearthdata.com/http//www.naturalearthdata.com/download/110m/cultural/ne_110m_admin_0_countries.zip
.. _package raster: https://rspatial.org/raster/RasterPackage.pdf
.. _package stars: https://cran.r-project.org/web/packages/stars/index.html
.. _page présente une comparaison: https://github.com/r-spatial/stars/wiki/How-%60raster%60-functions-map-to-%60stars%60-functions?utm_source=pocket_mylist
.. _page web dédiée à sf: https://r-spatial.github.io/sf/
