..  _divers:

Auteur : Paul Passy

Licence : |cc_by_nc_sa|

.. |cc_by_nc_sa| image:: figures/Cc-by-nc-sa_icone.png
              :width: 80 px

Divers
=========

Dans ce chapitre vous trouverez quelques outils qui ne sont pas à proprement parler dédiés à la géomatique mais qui peuvent s'avérer utile dans un contexte d'analyse de données géospatiales.

.. contents:: Table des matières
    :local:

..  _fouille-texte:

Fouiller un fichier texte
--------------------------

Dans certains traitements il peut être utile d'aller récupérer une valeur qui se trouve dans un fichier texte, comme un fichier de paramètres ou un fichier de métadonnées. C'est notamment le cas lorsque nous développons un script avec R ou Python et que nous avons besoin de récupérer une valeur stockée dans un fichier texte annexe.

..  _fouille-texte-R:

Fouille avec R
****************
Version de R : 4.8.1

Dans cet exemple, nous allons voir comment récupérer le niveau de produit d'une image Landsat stocké dans un fichier de métadonnées associé à l'image traitée. Dans ce fichier au format texte (*MTL.txt*), l'information recherchée se trouve à la ligne nommée *PROCESSING_LEVEL*. Dans un premier temps, nous allons donc récupérer les caractères de cette ligne et les stocker dans une variable que nous nommons *niveau*. Cette récupération se fait à l'aide de la méthode ``grep`` qui se trouve de base dans R. Cette méthode prend en entrée la chaîne de caractères recherchée et le fichier dans lequel chercher cette chaîne.

.. code-block:: R

    niveau <- grep("PROCESSING_LEVEL", readLines('LC08_L2SP_037034_20201027_20201106_02_T1_MTL.txt'), value = TRUE)

Cette variable *niveau* contient l'intégralité de la ligne contenant la chaîne de caractères *PROCESSING_LEVEL*, à savoir *PROCESSING_LEVEL = "L2SP"* avec les espaces et la tabulation du début de ligne. Il convient donc de nettoyer cette chaîne afin de ne conserver que le niveau *L2SP* (sans guillemets ni espaces). Pour cela nous utilisons la méthode ``str_replace()`` qui se trouve dans la librairie ``stringr``. Cette méthode prend en entrée la chaîne de caractères à modifier, les caractères à remplacer et les caractères de remplacement. Ici, nous commencerons par enlever les caractères "PROCESSING_LEVEL = ", c'est-à-dire remplacer cette chaîne par rien.

.. code-block:: R
    
    library(stringr)
    niveau <- str_replace(niveau, 'PROCESSING_LEVEL = ', '')

Nous mettons en fait à jour notre variable *niveau* en remplaçant la chaîne *PROCESSING_LEVEL =* (avec un espace après le égal) par aucun caractère comme indiqué par les guillemets vide *''*. Notre variable *niveau* contient maintenant le texte *"L2SP"* avec encore les guillemets et la tabulation. Il ne reste plus qu'à procéder de même pour enlever ces deux ensembles de caractères.

.. code-block:: R

    niveau <- str_replace(niveau, '"', '')
    niveau <- str_replace(niveau, '    ', '')

Notre variable *niveau* ne contient maintenant plus que le niveau *L2SP*.
