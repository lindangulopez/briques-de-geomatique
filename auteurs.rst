..  _auteurs:

Auteurs et contributeurs
=========================

Auteur principal
------------------   
.. image:: figures/fig_photo_PP.jpg
   :width: 5 em
   :target: https://cv.archives-ouvertes.fr/paul-passy
   :align: left
              
`Paul Passy`_, Maître de Conférences en Télédétection et SIG au sein du Département de Géographie (`UMR PRODIG`_) de l'Université de Paris. Je m'intéresse aux applications de la géomatique, aussi bien sur le côté traitement vecteur que raster, pour le suivi de l'environnement au sens large. J'utilise ces outils dans le cadre de mes recherches, notamment sur le cycle de l'eau, et pour mes enseignements de la L2 au M2.

Contributeurs
------------------

.. image:: figures/fig_photo_ST.jpg
   :width: 5 em
   :target: https://github.com/sylsta
   :align: left
   
`Sylvain Théry`_, ingénieur d'études au CNRS au sein de l'UMR `ART-Dev`_ basée à Montpellier. Sylvain s'intéresse plus particulièrement aux SIG, au `développement Python`_, notamment dans QGIS, et à la gestion des bases de données. Ses domaines d'applications sont le suivi de la qualité de l'eau ainsi que la géographie du développement.

.. _Paul Passy: https://cv.archives-ouvertes.fr/paul-passy
.. _UMR PRODIG: https://www.prodig.cnrs.fr/
.. _Sylvain Théry: https://art-dev.cnrs.fr/index.php/le-laboratoire/les-membres-de-l-unite/membres-permanents/212-thery-sylvain
.. _ART-Dev: https://art-dev.cnrs.fr/
.. _développement Python: https://github.com/sylsta
