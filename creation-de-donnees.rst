..  _creation-de-donnees:

Auteur : Paul Passy

Licence : |cc_by_nc_sa|

.. |cc_by_nc_sa| image:: figures/Cc-by-nc-sa_icone.png
              :width: 80 px

Création de données
====================

Dans un projet géomatique il est toujours nécessaire de créer de nouvelles données pour répondre à la question initiale du projet. Cette création de données peut se faire par digitalisation manuelle à partir d'un raster géoréférencé ou par export *en dur* d'un résultat de sélection par exemple.

.. contents:: Table des matières
    :local:

..  _digit:

Digitalisation
-----------------

Dans certains cas, nous pouvons être amenés à créer nous mêmes de la donnée en digitalisant des formes depuis un fond de carte géoréférencé. Dans certains domaines, ce processus de digitalisation sera très utilisé mais pour d'autres, cette opération sera très rare.

Les principes de base de la digitalisation sont faciles à comprendre et les logiciels de SIG proposent tous une fonction le permettant. Par contre, dès que nous souhaitons digitaliser un grand nombre d'entités, le travail se complexifie singulièrement. Dans ce cas, il faudra faire attention à la topologie entre les entités et entre les différentes couches. Heureusement, des outils nous aident à gérer ces aspects.

Digitalisation avec QGIS : les fondamentaux
********************************************
Version de QGIS : 3.16.1

Dans cet exemple, nous allons digitaliser quelques éléments relatifs à l'usage du sol sur une portion de l'Île de Ré, sur la façade Atlantique française. En fond de carte, nous afficherons la BD Topo de l'IGN et nous nous baserons sur ce fond pour digitaliser les éléments suivants : bâtiment, espace arboré et parking.

La première étape est de réfléchir au format de la donnée que nous souhaitons construire. D'abord d'un point de vue géométrique, allons-nous construire une couche vecteur de type points, lignes ou polygones ? Ensuite, quels attributs allons-nous associer à chacune des entités digitalisée ? Et quelle sera la nature de ces attributs ?

Une fois ces éléments définis, nous déciderons du système de coordonnées dans lequel nous allons digitaliser notre couche. Et enfin nous pourrons passer à la digitalisation proprement dite.

Initialisation de la couche
++++++++++++++++++++++++++++

Premièrement, nous commençons par afficher en fond les ortho-photos de l'IGN et à zoomer sur la partie de l'Île de Ré que nous souhaitons digitaliser. Nous pouvons nous référer à la fiche correspondante pour afficher ces ortho-photos.

Comme nous travaillons sur la France métropolitaine, nous décidons de travailler dans le système Lambert 93 (EPSG 2154). Les éléments digitalisés seront de type surfacique, nous allons donc digitaliser des polygones. À chacun de ces polygones nous allons associer les attributs suivants 

* un champ "*Nature*" dans lequel nous renseignerons si le polygone correspond à un bâtiment, à un espace arboré ou à un parking. Ce champ sera donc de type "*texte*".
* un champ "*superficie_m2*" dans lequel nous stockerons la superficie du polygone en mètres carrés. Ce champ sera donc de type "*réel*".

Dans QGIS, nous commençons par aller dans le menu *Couche* > *Créer une couche* > *Nouvelle couche GeoPackage*. La fenêtre suivante s'ouvre.

.. figure:: figures/fen_creation_couche1_qgis.png
    :width: 25em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: creation-couche1
    
    Création d'une couche GeoPackage dans QGIS.

Dans le champ *Base de données* nous spécifions le chemin et le nom du fichier GeoPackage que nous allons créer. Appelons le *digit_ile_de_re*. Dans le champ *Nom de la table*, nous définissons le nom de la table que nous allons créer dans ce GeoPackage, ici *usage_sol*. Dans le champ *Type de géométrie* nous définissons la géométrie de la couche, ici *Polygone*.

.. note::

	Notons bien qu'ici nous créons un fichier *GeoPackage* qui est une sorte de mini base de données. Ce GeoPackage doit être nommé par l'utilisateur. Au sein de ce GeoPackage, donc au sein de cette base de données, il est possible de créer plusieurs *couches* qui sont aussi appelées *tables*. Dans notre cas, nous n'avons qu'une table, celle de l'usage du sol. Mais nous pourrions y ajouter une autre table dans laquelle nous digitaliserions les routes par exemple.

Il est ensuite nécessaire de définir le système de coordonnées de la couche. Pour le définir facilement, nous cliquons sur l'icône |icone_choix_SCR|. La fenêtre de choix du SCR s'affiche alors.

.. |icone_choix_SCR| image:: figures/icone_choix_SCR.png
              :width: 25 px

.. figure:: figures/fen_choix_scr.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: choix-scr
    
    Choix d'un système de coordonnées.

Dans la champ *Filtre* nous entrons le code EPSG du Lambert 93, à savoir *2154*. Il suffit ensuite de double cliquer sur la ligne correspondante dans le panneau *Systèmes de Coordonnées de Références Prédéfinis*. La fenêtre précédente se met à jour avec le bon système de coordonnées.

Nous pouvons passer à la définition de la table attributaire associée à cette couche dans le panneau *Liste des champs*. Tout d'abord nous nommons le champ (*nom*). Nous le nommons *nature*. Ce champ est de *Type* *Donnée texte*. Le champ *Longueur maximale n'est pas obligatoire* mais nous pouvons le définir à *50*. Ça signifie que le champ *nature* ne pourra stocker plus de 50 caractères. Une fois ces réglages effectués, nous pouvons cliquer sur *Ajouter à la liste des champs*. La fenêtre se met à jour comme suit.

.. figure:: figures/fen_creation_couche2_qgis.png
    :width: 25em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: creation-couche2
    
    Définition des champs.

Nous recommençons l'opération pour le champ *superficie_m2*, qui sera de type *Nombre entier*. Lorsque tout est bien réglé, nous obtenons la fenêtre suivante.

.. figure:: figures/fen_creation_couche3_qgis.png
    :width: 25em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: creation-couche3
    
    Couche prête à être créée.

Nous cliquons sur *OK* et notre couche apparaît dans QGIS. Cette couche est encore vide, il n'y a ni entité spatiale, ni ligne dans la table attributaire.

.. note::

	Notons que les *Type* de champ les plus utilisés sont *Donnée texte* pour les données textuelles, *Nombre décimal (réel)* pour les données numériques qui font appel à des nombres décimaux comme les superficies, les densités, les altitudes ... et *Nombre entier* pour les données numériques entières comme les identifiants, les populations, les dénombrements ...

Digitalisation des entités
++++++++++++++++++++++++++++

Nous pouvons maintenant passer à la digitalisation proprement dite. Nous allons en fait "décalquer" les entités qui nous intéressent. Nous commencerons par un *bâtiment*, plus précisément une des maisons visibles sur l'ortho-photo. Assurons nous au préalable que la couche que nous venons de créer se trouve au dessus du fond de carte IGN dans le panneau des couches, sinon cette couche ne sera pas visible dans le panneau central.

Tout d'abord, il est nécessaire de passer cette couche en *mode édition*. Pour cela, nous sélectionnons notre couche dans le panneau des couches (elle se retrouve surlignée en bleu), puis par un clic droit, nous sélectionnons le menu *Basculer en mode édition*. L'icône à gauche de la couche s'orne d'un crayon.

.. figure:: figures/fig_couche_mode_edition.png
    :width: 20em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: couche_mode_edition
    
    Couche prête à être éditée.

Nous zoomons maintenant sur le bâtiment à digitaliser. La question du niveau de zoom et de la précision de la digitalisation reste ouverte. C'est à l'utilisateur de choisir un niveau de précision qui lui semble pertinent selon l'étude menée. Les outils dont nous allons avoir besoin se trouvent dans la barre d'outils de digitalisation |icone-barre-digit|.

.. |icone-barre-digit| image:: figures/icone_barre_outils_digit.png
              :width: 210 px

La digitalisation s'initie avec le menu *Ajouter une entité polygonale* |icone-ajout-entite|. Le curseur prend alors une forme de cible. La digitalisation se fait en traçant un polygone autour de l'objet choisi par une succession de clics gauches. Le polygone en cours de création apparaît en rouge.

.. |icone-ajout-entite| image:: figures/icone_ajout_entite.png
              :width: 25 px

.. figure:: figures/fig_digit_en_cours.png
    :width: 20em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: digit-en-cours
    
    Polygone en cours de digitalisation.

Le polygone se ferme par un clic droit. Notons que le clic droit n'ajoute pas de nouveau sommet, ainsi le polygone se ferme réellement là où nous avons cliqué gauche pour la dernière fois. Lorsque le polygone est fermé, une fenêtre nous invitant à entrer les attributs apparaît automatiquement.

.. figure:: figures/fen_table_apres_digit.png
    :width: 25em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: digit-table
    
    Invitation à remplir les attributs.

Dans le champ *nature* nous pouvons entrer *bâtiment*, et nous laissons le champ *superficie_m2* libre pour l'instant. Nous renseignerons tous les champs de superficie à la fin. Nous avons bien maintenant une entité associée à une ligne de la table attributaire. Le champ *nature* est bien renseignée comme nous le souhaitons et le champ *superficie_m2* est resté vide (*NULL*).

.. figure:: figures/fig_digit_entite_table_attributs.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: digit-entite-attributs
    
    L'entité digitalisée et ses attributs.

Si nous ne sommes pas satisfaits de la forme que nous avons donnée à l'entité, il est possible de déplacer, supprimer ou ajouter un nœud grâce à l'*Outils de nœud* |icone-outil-noeud|. Une fois cet outil sélectionné, le curseur prend la forme d'une croix. Et lors du survol d'un polygone, son contour se surligne en rouge et ses nœuds apparaissent.

.. figure:: figures/fig_edition_noeuds.png
    :width: 25em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: digit-edition-noeud
    
    Édition des nœuds d'un polygone.

Il est alors possible e déplacer un nœud avec le curseur de la souris ou d'en supprimer un en le sélectionnant puis en pressant la touche "*suppr*" de son clavier. Il est aussi possible d'ajouter un nœud entre deux nœuds existants en double cliquant sur la portion de ligne où nous souhaitons ajouter un nœud.

.. |icone-outil-noeud| image:: figures/icone_outil_noeud.png
              :width: 35 px

.. note::

	Nous remarquons la présence d'un champ *fid* que nous n'avons jamais défini. Ce champ est en fait un champ obligatoire de toute couche au format GeoPackage. Il s'agît d'un identifiant unique qui permet d'identifier chaque entité. Ce champ ne doit pas être modifié manuellement, nous n'y touchons pas et ne nous en occupons pas. D'ailleurs nous voyons qu'il sera mis à jour automatiquement par QGIS.

Nous pouvons maintenant recommencer l'opération pour les autres polygones, au moins un *parking* et un *zone arborée*.

.. figure:: figures/fig_digit_entite_table_attributs_2.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: digit-entite-attributs-2
    
    Les entités et leurs attributs.

Il est maintenant temps de mettre à jour le champ *superficie_m2* à l'aide de la *Calculatrice de champ*. Une fois les entités digitalisées et les champs mis à jour, nous pouvons enregistrer notre couche |icone-enregistrer| et quitter le mode édition en recliquant sur |icone-edition|.

.. |icone-enregistrer| image:: figures/icone_enregistrer.png
              :width: 25 px

.. |icone-edition| image:: figures/icone_edition.png
              :width: 25 px

.. warning::

	Il est possible, et même recommandé, de sauvegarder régulièrement sa couche en cours d'édition en cliquant sur l'icône d'enregistrement.

Gagner en efficacité avec les formulaires
++++++++++++++++++++++++++++++++++++++++++++

QIGS offre un outil très pratique pour nous aider dans la digitalisation : le gestionnaire de formulaires. Dans notre exemple, nous avons des entités de trois natures différentes. Mais à chaque fois nous devons taper à la main dans le champ *nature* les mots *bâtiment*, *parking* ou *zone arborée*. Or nous ne sommes pas à l'abri d'une faute de frappe, ou d'écrire *batiment* au lieu de *bâtiment* ou de mettre une majuscule ou un pluriel... surtout si nous sommes plusieurs utilisateurs à travailler sur cette même couche. Le risque est de se retrouver au final, non pas avec trois natures différentes mais une multitude (*bâtiment*, *batiment*, *Parkings*, *parkign*, ...)

Pour pallier à ça, nous pouvons utiliser un *formulaire*. Concrètement, un tel formulaire forcera l'utilisateur à choisir seulement parmi les valeurs possibles pour remplir un champ donné. Pour définir le formulaire, nous faisons un clic droit sur la couche puis nous sélectionnons *Propriétés*. Dans la fenêtre qui s'ouvre, nous allons dans l'onglet *Formulaire...*

.. figure:: figures/fen_proprietes_formulaire.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: prop-formulaire
    
    L'onglet de définition des formulaires.

Nous commençons par définir le formulaire à associer au champ *nature*. Nous sélectionnons donc *nature* dans le menu déroulant *Fields*. Le panneau de droite se met à jour. Dans le panneau *Type d'outils*, nous sélectionnons *Liste de valeurs*. Dans le tableau qui suit nous définissons les différentes valeurs possibles. La colonne *Valeur* contiendra les valeurs standardisées et la colonne *Description* contiendra une description de la valeur, le plus simple est de mettre la même chose dans les deux colonnes. Nous pouvons ainsi paramétrer ce tableau comme défini sur la figure suivante.

.. figure:: figures/fen_formulaire_texte.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: formulaire-texte
    
    Définition des différentes valeurs possibles pour le champ nature.

Notons que nous pouvons également cocher la case *Non null* dans le panneau *Contraintes*. Ainsi l'utilisateur sera obligé de renseigner le champ *nature*.

Nous pouvons en profiter pour créer un autre formulaire pour le champ *superficie_m2*. Nous sélectionnons maintenant ce champ dans le menu déroulant *Fields*. Il ne s'agit plus de valeurs à sélectionner mais nous souhaitons que QGIS calcule de lui même automatiquement la superficie de l'entité au moment de sa création. Pour cela, nous allons dans le panneau *Défauts* et nous cliquons sur l'icône |icone-calcul-champ| à droite du champ *Valeur par défaut*.

.. |icone-calcul-champ| image:: figures/icone_calcul_champ.png
              :width: 25 px

La fenêtre de *Calculateur d'Expressions* s'ouvre. Dans le panneau *Expression* nous allons entrer l'expression qui permet de calculer une superficie. Pour cela, dans le panneau central, nous déroulons le menu *Géométrie* et nous double cliquons sur *$area*. Cette fonction indique à QGIS de calculer la superficie de l'entité correspondante. Le calculateur d'expressions ressemble alors à la fenêtre suivante.

.. figure:: figures/fen_calculateur_expressions_area.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: expression-area
    
    Définition du calcul de l'aire de façon automatique.

En cliquant sur *OK* nous revenons à la fenêtre des formulaires et le champ *Valeur par défaut* a été mis à jour. Nous n'oublions pas de cocher la case *Appliquer la valeur par défaut sur la mise à jour*.

.. figure:: figures/fen_formulaire_area.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: formulaire-area
    
    Formulaire de calcul automatique de la superficie.

Nous n'avons plus qu'à cliquer sur *Appliquer* et *OK*.

Nous allons voir l'intérêt de ces formulaires en digitalisant une nouvelle entité, une *zone arborée* par exemple. Nous reprenons l'outil d'ajout d'entités, nous digitalisons notre espace arborée et au moment de la fermeture du polygone, la fenêtre de mise à jour des attributs apparaît. Mais la fenêtre présente un aspect légèrement différent maintenant.

.. figure:: figures/fen_digit_avec_formulaire_1.png
    :width: 25em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: digit-formulaire-1
    
    Ouverture du formulaire après création d'entité.

Nous voyons que le champ *nature* n'est plus un champ de texte libre mais une liste déroulante. Et le champ *superficie_m2* a été automatiquement mis à jour avec la superficie. Il nous suffit maintenant de dérouler la liste du champ *nature* et de sélectionner celui qui nous convient.

.. figure:: figures/fen_digit_avec_formulaire_2.png
    :width: 25em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: digit-formulaire-2
    
    Sélection de la nature dans le menu déroulant.

Ainsi, nous gagnons du temps et évitons des erreurs inévitables.


..  _creation-de-donnees-par-export:

Créer des données par export d'une sélection
---------------------------------------------

Un moyen simple et fréquent de "créer" une nouvelle couche géographique est d'exporter une sous partie d'une couche déjà existante. Il est fréquent de n'avoir besoin que d'une petite partie d'une couche fournie par un tiers. La partie d'intérêt peut être sélectionnée suite à une sélection par attributs ou suite à une sélection spatiale. Il est alors possible d'exporter le résultat de cette sélection dans une nouvelle couche.

Exporter une sélection avec QGIS
*********************************
Version de QGIS : 3.16.1

La procédure d'export d'une sélection avec QGIS est très simple. Dans notre exemple, nous allons exporter les limites administratives australiennes vers une nouvelle couche *australie.gpkg* depuis la couche des limites administratives du monde disponible sur `Natural Earth`_.

.. _Natural Earth: https://www.naturalearthdata.com/http//www.naturalearthdata.com/download/50m/cultural/ne_50m_admin_1_states_provinces.zip

Une fois cette couche chargée dans QGIS, nous commençons par sélectionner les entités administratives australiennes (:numref:`select-australie`).

.. figure:: figures/fig_selection_australie.png
    :width: 25em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: select-australie
    
    Sélection des entités administratives australiennes.

L'export de cette sélection se fait par un clic droit sur la couche dans le panneau de couches et en sélectionnant :guilabel:`Exporter` > :guilabel:`Sauvegarder les entités sélectionnées sous...` La fenêtre d'export s'ouvre (:numref:`select-export`).

.. figure:: figures/fen_export_selection.png
    :width: 25em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: select-export
    
    Exportation de la sélection vers une nouvelle couche.

Dans le champ ``Format`` nous sélectionnons le format d'export. Dans le champ ``Nom de fichier``, nous spécifions le chemin d'export ainsi que le nom du fichier. Dans le champ ``Nom de la source``, nous spécifions un nom de couche. Nous pouvons également choisir un SCR dans le champ ``SCR``. Puis nous cliquons sur :guilabel:`OK`. La nouvelle couche apparaît automatiquement dans QGIS.
