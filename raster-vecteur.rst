..  _raster-vecteur:

Auteur : Paul Passy

Licence : |cc_by_nc_sa|

.. |cc_by_nc_sa| image:: figures/Cc-by-nc-sa_icone.png
              :width: 80 px

Combinaison de rasters et vecteurs
====================================

Il est possible de croiser des couches rasters et vecteurs afin d'en tirer une nouvelle information. Les deux formats dialoguent très bien et de nombreux outils permettent de croiser ces deux types de données. Nous en verrons ici quelques un comme les statistiques zonales ou la conversion d'un raster vers un vecteur et vice versa.

.. warning::
	Pour tous les outils qui font intervenir une superposition de rasters et de vecteurs, il est vivement conseillé de travailler avec des couches partageant le même système de coordonnées de référence.

.. contents:: Table des matières
    :local:

.. _stats-zonales:

Statistiques zonales
---------------------

Cette partie présente la manière de procéder pour calculer des statistiques zonales. Par statistiques zonales nous entendons le fait de calculer des statistiques comme une moyenne, une médiane, un écart-type ... à partir d'une couche raster et pour chaque entité d'une couche vecteur de type polygones. Le principe est de récupérer les valeurs de tous les pixels se trouvant sous un polygone et d'en calculer une statistique descriptive comme la moyenne ou autre (:numref:`stats_zonales_theorie`).

.. figure:: figures/fig_stats_zonales_theorie.png
    :width: 25em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: stats_zonales_theorie
    
    Principe du calcul de statistiques zonales par recouvrement d'une couche raster (en gris) par une couche vecteur de type polygone en vert et de type ligne en rouge.

Sur la figure précédente (:numref:`stats_zonales_theorie`), nous pouvons par exemple calculer une statistique zonale pour le polygone vert. Le principe est dans un premier temps de repérer tous les pixels recouverts par le polygone vert lorsque la couche vecteur de polygones est superposée à la couche raster. Puis dans un second temps de calculer la statistique souhaitée sur ces pixels. Cette statistique peut être le maximum, le minimum, la moyenne, la variance, un percentile... Il est également possible, en théorie, de calculer des statistiques zonales selon le même principe mais pour une couche vecteur de type *lignes*. Dans ce cas aussi, les pixels recouverts par la ligne seront repérés puis une statistique sera calculée sur ces pixels. Notons que les statistiques zonales sous des lignes ne sont pas calculables avec tous les outils.

.. note::
	Les vecteurs de type polygones, et à fortiori de type lignes, ne recouvrent pas tous les pixels à 100 %. En bordure, des pixels se retrouvent recouverts par le polygone en partie seulement. Faut-il alors les compter dans les statistiques zonales ? La question reste entière et il n'est pas toujours simple de savoir ce que fait chaque algorithme selon les outils. Dans la plupart des cas, nous fermons les yeux sur cette question...

Cette manipulation est assez fréquente en SIG. Par exemple, si vous disposez d'un raster de modèle numérique de terrain (MNT) et d'une couche vectorielle présentant des polygones de bassins-versants, grâce à cette manipulation vous pourrez calculer l' altitude moyenne / médiane / maximum ... pour chacun de vos bassins.
Dans cet exemple, nous allons calculer l'altitude moyenne pour chaque bassin-versant unitaire du bassin de la `Roya`_ dans les Alpes-Maritimes. Pour effectuer ce calcul, nous disposons du MNT de la Roya (*srtm_roya.tif*) et d'une couche vecteur de polygones (*hydro_bv_roya.gpkg*).

Cette manipulation peut s'effectuer avec tous les logiciels de SIG ainsi qu'avec R ou Python.

.. _stats-zonales-qgis:

Statistiques zonales avec QGIS
********************************

Version de QGIS : 3.16.1

Avec des polygones
++++++++++++++++++++

Il faut tout d'abord charger dans QGIS votre MNT et votre couche des bassins-versants. La manipulation s'effectue alors simplement en cherchant le module nommé ``Statistiques de zone`` dans la Boîte à outils de traitements de QGIS. À l'ouverture de ce module, la fenêtre de configuration suivante apparaît (:numref:`qgis-stats-zonales-param`).

.. figure:: figures/fen_stats_zonales.png
    :width: 25em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: qgis-stats-zonales-param
    
    Paramétrage des statistiques zonales dans QGIS.

Dans le champ ``Couche source``, il faut définir la couche vecteur sur laquelle nous voulons calculer les statistiques, à savoir *hydro_bv_roya*. Dans le champ ``Couche raster``, nous spécifions le raster depuis lequel nous calculons les statistiques, ici *srtm_roya*. Notons que dans le cas d'un raster multi-bandes, comme une image satellite multispectrale par exemple, nous pouvons choisir la bande à étudier dans le champ ``Bande raster``. Dans le champ ``Préfixe de la colonne en sortie``, nous pouvons mettre *alti* pour altitude. Le *underscore* va permettre de nommer le champ résultant *alti_moyenne*.

Dans le champ ``Statistiques à calculer``, nous cliquons sur l'icône |icone_browse| et nous sélectionnons la statistique que nous désirons parmi toutes celles proposées, ``moyenne`` dans notre cas. Enfin, dans le champ ``Statistiques de zone``, nous spécifions le chemin d'export de la couche vecteur qui contiendra les statistiques. En effet, une nouvelle couche sera créée, il n'est pas possible de mettre directement à jour la couche. La nouvelle couche apparaît automatiquement et elle contient un nouveau champ nommé *alti_mean* qui contient l'altitude moyenne de chaque bassin-versant.

Avec des polylignes
++++++++++++++++++++

Il est possible de calculer des statistiques zonales le long de polylignes dans QGIS en utilisant un outil issu de GRASS GIS. Comme les outils GRASS sont interfacés nativement par QGIS, il n'y a aucune difficulté. Dans l'exemple, nous allons calculer quelques statistiques zonales d'altitude le long du réseau hydrographique du bassin de la Roya (*Roya_hydro.gpkg*). Nous commençons par charger la couche du réseau hydrographique sous forme de polylignes. Dans le fichier employé, chaque tronçon de rivière compris entre une source et la première confluence ou entre deux confluences est considéré comme une entité. Une fois les couches chargées, nous ouvrons le menu ``v.rast.stats`` qui se trouve dans la :menuselection:`Boîte à outils de traitements --> GRASS --> Vector (v.*)`. La fenêtre suivante s'ouvre (:numref:`qgis-stats-zonales-lignes`).

.. figure:: figures/fen_qgis_stats_zonales_lignes.png
    :width: 25em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: qgis-stats-zonales-lignes
    
    Statistiques zonales le long de lignes dans QGIS.

À la ligne ``Name of vector polygon map`` nous indiquons le nom de la couche de points pour laquelle nous souhaitons les statistiques, à savoir *Roya_hydro*. Bien qu'il soit mentionné *Polygon*, il est possible de mettre des lignes en entrée. À la ligne ``Name of raster map to calculate statistics from``, nous indiquons le raster à utiliser pour le calcul des statistiques, à savoir *srtm_roya*. La ligne ``Column prefix for new attribute columns`` est pratique. Il s'agît de définir le préfixe que prendront les colonnes de statistiques, ici *hydro*. Dans le panneau ``The methods to use``, nous précisons les statistiques à calculer en cliquant sur l'icône |icone_browse|. Pour l'exemple nous choisissons la moyenne (*average*) et l'écart-type (*stdev*). Enfin, à la ligne ``Rast stats`` nous indiquons un chemin et un nom pour la nouvelle couche de type lignes qui contiendra deux nouvelles colonnes dans la table attributaire, une pour les altitudes moyennes et une autre pour l'écart-type des altitudes de chaque tronçon de rivière. Nous pouvons la nommer *Roya_hydro_stats_alti.gpkg*. Puis nous cliquons sur :guilabel:`Exécuter`. La nouvelle couche, en tout point identique à la couche de lignes initiale, apparaît. Mais nous y retrouvons bien deux nouvelles colonnes dans la table attributaire.

.. _stats-zonales-qgis-multi:

Statistiques zonales sur raster multi-bandes avec QGIS
*********************************************************
Version de QGIS : 3.18.3

Il est possible de calculer des statistiques zonales sur plusieurs rasters à la fois en calculant ces statistiques sur un raster multi-bandes. Ce raster multi-bandes peut être un "vrai" multi-bandes au format *.tif* ou un bien un raster virtuel au format *.vrt*.

.. note::
	Ce module ne prend en entrée que des couches vecteurs de type polygones.

Dans cet exemple, nous allons calculer les réflectances moyennes sur 7 bandes spectrales Landsat 8 pour quelques communes du Val d'Oise.

Cette fonctionnalité nécessite d'installer au préalable une extension supplémentaire nommée *Zonal Statistics for Multiband Rasters*. Cette extension est expérimentale (mais fonctionne tout de même bien). Pour l'installer, il est nécessaire d'autoriser les extensions expérimentales en cochant la case ``Afficher les extensions expérimentales`` dans le menu :menuselection:`Extensions --> Installer/Gérer les extensions --> Paramètres`. Une fois cette option cochée, nous retournons dans l'onglet ``Toutes`` et nous recherchons cette extension dans la barre de recherche (:numref:`qgis-dymaxion-install`).

.. figure:: figures/fen_qgis_dymaxion_extension.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: qgis-dymaxion-install
    
    Installation de l'extension de calcul de statistiques zonales multi-bandes.

Une fois l'extension installée, elle apparaît comme un module dans la Boîte à outils de traitements sous l'entrée *Dymaxion Labs*. Nous chargeons ensuite le raster multi-bandes à analyser. Il peut être en *.tif* ou bien sous la forme d'un raster virtuel. Nous chargeons également la couche des entités polygonales sur lesquelles nous souhaitons calculer les statistiques.

Nous ouvrons cette extension en allant dans la :menuselection:`Boîte à outils de traitements --> Dymaxion Labs --> Raster Analysis --> Zonal statistics (multiband)`. Le menu suivant apparaît (:numref:`qgis-dymaxion-param`).

.. figure:: figures/fen_qgis_dymaxion_param.png
    :width: 25em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: qgis-dymaxion-param
    
    Calcul de statistiques zonales multi-bandes.

Dans le champ ``Raster layer`` nous indiquons le raster multi-bandes à analyser, ici *L8_stack*. Dans le champ ``Vector layer containing zones``, nous sélectionnons la couche vecteur de polygones, ici *communes_95_str*. Dans le champ ``Output column prefix`` nous pouvons spécifier un préfixe pour le nouveau champ qui sera calculé. Comme nous allons calculer la moyenne des réflectances, nous le nommons *moy_* . Enfin, dans le champ ``Statistics to calculate`` nous sélectionnons une ou plusieurs statistiques à calculer. Ici, nous ne calculons que la moyenne. Nous cliquons ensuite sur :guilabel:`Exécuter`.

Contrairement à l'outil interne à QGIS, les statistiques sont directement ajoutées à la couche vecteur d'entrée. Si nous ouvrons sa table attributaire, nous remarquons les nouveaux champs calculés à la fin. Ils se nomment *moy_b1_mean*, *moy_b2_mean*, etc

.. tip::
	Ce module peut s'avérer très pratique. Il peut même être pertinent de créer un raster multi-bandes spécialement pour utiliser cette extension. Par exemple, lorsque nous souhaitons calculer des primitives pour classifier des segments suite à une segmentation d'images, cette extension peut nous faire gagner du temps. Nous pouvons également l'utiliser sur un raster mono-bande si nous souhaitons "mettre à jour" une couche vecteur avec les statistiques zonales sans devoir en créer une nouvelle.


.. _stats-zonales-otb:

Statistiques zonales avec OTB
********************************
Version de OTB : 7.2.0

:ref:`logiciels-OTB` propose un module de calcul de statistiques zonales. Comme tous les modules OTB, il est utilisable via l'interface Monteveri ou via la ligne de commende *otbcli*. Dans l'exemple nous allons calculer l'altitude moyenne de quelques communes localisées dans le bassin-versant de la Roya.

.. warning::
	OTB ne permet pas de calculer des statistiques zonales sur autre chose que des polygones. 

Via Monteverdi
++++++++++++++++++++

Dans l'interface, le module dédié se nomme ``ZonalStatistics`` et se trouve dans le :menuselection:`Navigateur d'OTB-Applications --> Image Analysis --> ZonalStatistics`. Une fois sélectionné, le menu suivant s'ouvre (:numref:`otb_zonal_stats`).

.. figure:: figures/fen_otb_zonal_stats.png
    :width: 25em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: otb_zonal_stats
    
    Calcul de statistiques zonales avec OTB via Monteverdi.

À la ligne ``Input Image``, nous sélectionnons le raster sur lequel calculer les statistiques, à savoir *srtm_roya.tif*. Dans le panneau ``Type of input for the zone definitions``, nous spécifions si nos zones sont sous forme de fichier vecteur ou raster. Ici, nous travaillons avec une couche vecteur de type polygones représentant les communes, nous sélectionnons donc ``Input objects from vector data`` ; et à la ligne ``Input vector data`` nous indiquons où se trouve le fichier vecteur à utiliser. Si jamais le fichier vecteur n'est pas dans le même système de coordonnées de référence que la couche raster d'entrée, nous pouvons la reprojeter à la volée en mettant à ``ON`` l'option ``Reproject the input vector``. Nous devons ensuite choisir un type de sortie dans le panneau ``Format of the output stats``. Si nous choisissons une sortie en vecteur, nous sélectionnons ``Output vector data`` et nous le nommons à la ligne ``Filename for the output vector data``. La sortie peut être en geopackage. Puis nous cliquons sur :guilabel:`Execute`. Un fichier vecteur en tout point identique au fichier d'entrée est créé. Mais il y a maintenant cinq nouvelles colonnes dans la table attributaire :

 * *count* : le nombre de pixels sous chaque polygone
 * *mean_0* : la moyenne des pixels sous chaque polygone et dans la première bande du raster d'entrée (qu'une bande ici)
 * *stdev_0* : l'écart-type des pixels
 * *min_0* : le minimum des pixels
 * *max_0* : le maximum des pixels
 
Il n'y a pas le choix des statistiques calculées avec OTB.

.. tip::
	Si le raster en entrée est un :ref:`rasters-multi-bandes`, les statistiques zonales sont calculées sur toutes les bandes dans le même ordre que dans le raster. Les colonnes de statistiques ont alors un suffixe *_0*, *_1*, ...

Ce module propose également de sortir le résultat sous forme d'un raster plutôt que d'un vecteur. Le résultat est alors un raster multi-bandes où chaque bande est une statistique et dans le même ordre que présenté dans le cas d'une sortie vecteur. Cette sortie sous forme de raster peut être utile dans le cas où cette sortie sert d'entrée à une classification.

Via la ligne de commande
+++++++++++++++++++++++++

Le calcul des statistiques zonales est également possible via la ligne de commande *otbcli*. Pour une documentation détaillée, voire la référence sur `le site de OTB`_. La ligne de commande idoine prend la forme suivante.

.. code-block:: sh

    otbcli_ZonalStatistics -in srtm_roya.tif -inzone.vector.in roya_communes.gpkg -out.vector.filename roya_communes_alti.gpkg

Avec :
 * **otbcli_ZonalStatistics** : l'appel à la fonction dédiée
 * **-in srtm_roya.tif** : le raster duquel calculer les statistiques à utiliser en entrée
 * **-inzone.vector.in roya_communes.gpkg** : les zones à utiliser, ici sous forme de vecteur
 * **-out.vector.filename roya_communes_alti.gpkg** : le fichier de sortie, ici sous forme de vecteur


.. _stats-zonales-R:

Statistiques zonales avec R
********************************
Version de R : 4.1.2

Il est possible de calculer des statistiques zonales avec R. Il est nécessaire d'avoir au préalable chargé une couche raster et une couche vecteur avec des librairies dédiées. Dans les exemples ci-dessous, nous allons calculer l'altitude moyenne pour quelques communes du bassin-versant de la `Roya`_. Nous utiliserons le MNT fourni par le SRTM (*srtm_roya_L93.tif*) et une couche des communes au format geopackage sous forme de polygones (*Roya_communes.gpkg*).

Avec Stars et Sf
+++++++++++++++++

Si nous avons un raster stocké sous forme d'objet *Stars* et d'un vecteur stocké sous forme d'objet *sf*, la commande à utiliser est *aggregate()*. Cette commande prend en argument le raster à échantillonner, le vecteur contenant les zones et la statistique à calculer. Le résultat sera par défaut stocké en objet *Stars*. Cependant, il est souvent plus facile de gérer des objets de type *sf*. C'est pourquoi nous allons convertir la sortie de la commande *aggregate()* en objet *sf* grâce à *%>% st_as_sf()*. Le *%>%* signifie simplement que nous prenons le résultat de la commande précédent ce signe et que nous le "donnons à manger" à la commande qui suit ce signe.

.. code-block:: R

    library(stars)
    library(sf)
    # chargement du MNT et des communes
    mnt <- read_stars("srtm_roya_L93.tif")
    communes <- st_read("Roya_communes.gpkg")
    # calcul de la moyenne zonale et export du résultat dans un objet sf
    communes_alti <- aggregate(mnt, communes, FUN = mean) %>% st_as_sf()
    
    # jointure spatiale entre les communes initiales et les communes avec altitude
    communes_alti <- st_join(communes, communes_alti, join = st_equals)
    
    # pour peaufiner nous renommons la colonne contenant les altitutes moyennes
    names(communes_alti2)[names(communes_alti2)=="srtm_roya_L93.tif"] <- "altitude_moyenne"

Nous obtenons bien un objet *sf* qui contient les altitudes moyennes de chaque commune. L'inconvénient c'est que les champs attributaires de la couche vecteur initiale ont été perdus dans le processus. Il est nécessaire d'opérer une jointure spatiale pour les retrouver. Nous pouvons également renommer la colonne contenant les altitudes moyennes car par défaut elle a le nom de la couche raster initiale. Enfin, il ne semble pas possible de calculer plus d'une statistique à la fois (à explorer).

.. tip::
    Si le raster en entrée est multi-bandes, la statistique zonale sera calculée pour chaque bande et stockée dans la table attributaire dans le même ordre que les bandes du raster initial.
    
 
.. _raster-points:

Échantillonnage de raster selon des points
--------------------------------------------

Il est possible de superposer une couche vecteur ponctuelle sur une couche raster et de relever les valeurs des pixels recouverts par les points. Ainsi, nous ajoutons dans la table attributaire de la couche de points les valeurs des pixels du raster sous-jacents à chacun des points. Cette manipulation peut se faire selon différents outils. Cette manipulation est aussi connue sous son appellation en anglais *Point sampling tool*.

Dans les exemples suivants, nous releverons l'altitude à partir d'un MNT pour quelques points superposés à ce MNT (:numref:`point-sampling-tool`).

.. figure:: figures/fig_point_sampling_tool.png
    :width: 15em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: point-sampling-tool
    
    Points d'échantillonnage.

.. _raster-points-qgis:

Échantillonnage avec QGIS
***************************
Version de QGIS : 3.22.0

Nous commençons par charger dans QGIS le raster à échantillonner et la couche de points pour lesquels nous souhaitons relever les échantillons. Le menu à utiliser se trouve dans la :menuselection:`Boîte à outils de traitements --> Analyse raster --> Prélever des valeurs rasters`. Le menu suivant s'ouvre (:numref:`point-sampling-qgis`).

.. figure:: figures/fen_point_sampling_tool_qgis.png
    :width: 25em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: point-sampling-qgis
    
    Échantillonnage d'un raster selon une couche de points dans QGIS.

Dans le menu ``Couche source`` nous spécifions la couche de points à utiliser. Nous pouvons, si nous le souhaitons, cocher la case ``Entité(s) séléectionnée(s) uniquement`` si nous souhaitons simplement échantillonner certains points précédemment sélectionnés. Dans le menu ``Couche raster`` nous indiquons le raster à échantillonner. Nous pouvons personnaliser le ``Préfixe de la colonne en sortie``, ici *alti_* par exemple. Enfin, dans le champ ``Échantillonné`` nous indiquons un chemin et un nom pour la couche de points qui contiendra les valeurs du raster échantillonné.

Nous obtenons une couche de points identiques à la couche de points initial mais avec un nouveau champ intitulé *alti_1* où nous trouvons la valeur du pixel du MNT sous-jacent à chaque point.

.. note::
	Si notre MNT avait été multi-couches, nous aurions obtenu autant de nouveaux champs que de couches dans le raster, nommés *alti_1*, *alti_2*, ...


.. _raster-to-vecteur:

Raster vers vecteur
---------------------

Il est possible de convertir un raster en un vecteur. C'est une manipulation notamment utilisé pour transformer un raster d'occupation du sol en une couche vecteur sur laquelle il sera possible de calculer des superficies ou de faire des croisements avec d'autres couches géospatiales. C'est également utilisé à la suite de la construction d'un raster binaire mettant en avant un aspect particulier du terrain. La manipulation peut se faire selon différents outils qui peuvent proposer plus ou moins d'options. Dans les exemples suivants nous convertirons un (petit) raster d'occupation du sol.

Seuls certains outils proposent de convertir autre chose que des surfaces, comme des lignes par exemple. C'est le cas de l'outil proposé par GRASS GIS (:ref:`grass-lignes-vect`).

.. warning::
	Après une conversion de couche raster en couche vecteur, même si le résultat est visuellement propre, il peut y avoir des erreurs de topologie dans le vecteur résultat. Ces erreurs empêcheront d'effectuer des géotraitements à partir de ce vecteur. Il sera nécessaire de corriger les erreurs générées en port-traitement. Ces corrections peuvent se faire dans QGIS avec l'outil *Réparer les géométries*.

Raster vers vecteur dans QGIS
******************************

Version de QGIS : 3.20.1

Une fois le raster chargé dans QGIS, il est possible de le transformer une couche vecteur. Pour ce faire, nous allons dans le menu :menuselection:`Raster --> Conversion --> Polygoniser (raster vers vecteur)...` Le menu dédié s'ouvre (:numref:`qgis-rast-vect`).

.. figure:: figures/fen_qgis_raster_to_vecteur.png
    :width: 25em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: qgis-rast-vect
    
    Conversion d'un raster en vecteur dans QGIS.

Dans la ligne ``Couche source``, nous indiquons le raster à convertir, ici *xtr_usol_2011*. Nous pouvons choisir la bande à convertir dans le menu déroulant ``Numéro de bande``, mais ici nous n'avons qu'une bande dans notre raster. Nous indiquons le nom du champ qui sera créé et qui contiendra les valeurs des pixels à la ligne ``Nom du champ à créer``, nous pouvons indiquer *usol*. La case ``Use 8-connectedness`` permet de regrouper dans un même polygone deux entités qui ne se toucheraient que par un coin. Si cette case est décochée, deux entités se touchant par un coin seront considérées comme distinctes. Enfin, dans la ligne ``Vectorisé`` nous indiquons un chemin et un nom pour le fichier vecteur qui sera créé. Notons que ce menu est en fait un simple interface d'une commande GDAL comme nous pouvons le voir dans le panneau ``Console GDAL/OGR``. Nous cliquons sur :guilabel:`Exécuter` pour lancer la conversion. Nous obtenons bien un fichier vecteur de type polygones (:numref:`rsl-rast-vect` B) possédant une table attributaire associée.

.. figure:: figures/fig_rast_vect.png
    :width: 25em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: rsl-rast-vect
    
    Le raster initial (A) et le résultat de la conversion en vecteur (B).

.. note::
	Nous constatons qu'avec ce menu il n'est possible d'obtenir que des vecteurs de type *polygones* (i.e. surfacique), il n'est pas possible de vectoriser des lignes comme un réseau hydrographique par exemple.

Raster vers vecteur avec SCP
******************************

Version de QGIS : 3.20.1

Version de SCP : 7.9.5

Le module SCP de QGIS (:ref:`logiciels-SCP`) propose lui aussi un menu de conversion de raster vers vecteur. Une fois le raster chargé dans QGIS, nous allons dans le menu :menuselection:`SCP --> Post-traitement --> Classification vers vecteur`. Le menu s'ouvre (:numref:`scp-rast-vect`). Dans le panneau ``Clasification vers vecteur``, nous sélectionnons le raster à convertir. Si besoin nous rafraîchissons la liste à l'aide de l'icône dédiée |icone_actualiser|. Si le raster à convertir est issu d'une classification réalisé avec SCP nous pouvons récupérer directement son style en cochant la case ``Utiliser les codes de la liste des signatures``. Nous cliquons ensuite sur :guilabel:`Lancer`.

.. figure:: figures/fen_SCP_raster_to_vecteur.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: scp-rast-vect
    
    Conversion d'un raster en vecteur avec SCP.

Nous constatons que le fichier vecteur créé est découpé en bandes horizontales. C'est un artefact utilisé par ce module pour accélérer le traitement. Il suffit de regrouper les polygones par classe d'occupation du sol pour s'affranchir à postériori de cet artefact.


Raster vers vecteur avec GRASS GIS dans QGIS
**********************************************

Version de QGIS : 3.20.1

Le logiciel GRASS GIS (:ref:`logiciels-grass`) propose un module de transformation de raster vers vecteur. Comme GRASS GIS est interfacé en natif par QGIS, ce module est utilisable directement depuis QGIS. Une fois le raster chargé, nous sélectionnons le menu *r.to.vect* dans la :menuselection:`Boîte à outils de traitements --> GRASS --> Raster --> r.to.vect`. Le menu GRASS apparaît (:numref:`grass-qgis-rast-vect`).

Cas pour des polygones
++++++++++++++++++++++++

.. figure:: figures/fen_grass_qgis_raster_to_vecteur.png
    :width: 25em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: grass-qgis-rast-vect
    
    Conversion d'un raster en vecteur avec GRASS GIS dans QGIS.

À la ligne ``Couche raster en entrée`` nous indiquons le raster à convertir, ici *xtr_usol_2011*. Dans le menu ``Type d'entités`` nous spécifions que nous allons convertir des surfaces (i.e. des polygones) en sélectionnant *area*. Dans le champ ``Nom de la colonne d'attribut`` nous pouvons indiquer le nom que prendra le champ qui contiendra les valeurs prises du raster, *usol* par exemple. Nous pouvons laisser les cases suivantes décochées. Enfin, dans ``Vectorisé``, nous indiquons un chemin et un nom pour le raster qui sera créé. Puis nous cliquons sur :guilabel:`Exécuter`. Le fichier converti apparaît automatiquement. Plusieurs champs sont créés, mais seul le champ que nous avons baptisé *usol* nous importe. Les autres champs peuvent être supprimés.

.. _grass-lignes-vect:

Cas pour des lignes
++++++++++++++++++++++++

Un aspect intéressant de cet outil de GRASS GIS est qu'il permet de vectoriser des lignes. C'est tout à fait intéressant dans le cas où nous avons un raster présentant un réseau hydrographique, par exemple issu d'une extraction automatique de réseau hydrographique à partir d'un modèle numérique de terrain. Le point délicat d'une telle conversion de lignes est que le raster de départ doit rigoureusement représenter des lignes. C'est-à-dire que chaque pixel doit toucher au plus deux pixels (et pas un de plus). Heureusement, GRASS propose également un outil qui permet *d'amincir* un raster de lignes nommé *r.thin*. Ainsi, la conversion d'un raster de lignes va se faire en deux temps. D'abord, l'amincissement avec *r.thin* puis la conversions proprement dite avec *r.to.vect*. Dans l'exemple, nous allons convertir en vecteur le réseau hydrographique de la région de la `Roya`_ que nous avons en raster. Nous commençons par charger ce raster dans QGIS puis nous ouvrons le menu *r.thin* via la :menuselection:`Boîte à outils de traitements --> GRASS --> Raster --> r.to.thin`. Le menu suivant apparaît (:numref:`grass-qgis-thin`).

.. figure:: figures/fen_grass_qgis_thin.png
    :width: 25em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: grass-qgis-thin
    
    Amincissement d'un raster de lignes avec GRASS GIS dans QGIS.

Dans le menu ``Input raster layer to thin`` nous sélectionnons le raster à amincir *hydro_Roya*. Le ``Nombre maximum d'itérations`` doit être d'autant plus élevé que le raster a besoin d'être aminci, la valeur de *200* peut être laissée par défaut. Enfin, à la ligne ``Aminci``, nous indiquons le chemin et le nom du raster aminci qui sera créé. Il ne reste plus qu'à cliquer sur :guilabel:`Exécuter`. Le raster résultat est très similaire au raster initial, seuls quelques pixels ont été enlevés (:numref:`grass-qgis-thin-rsl`).

.. figure:: figures/fig_raster_thin.png
    :width: 25em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: grass-qgis-thin-rsl
    
    Raster initial (A) et raster après amincissement (B).

Nous constatons sur la figure précédente (:numref:`grass-qgis-thin-rsl`) que le pixel en rouge du raster initial touchait 3 autres pixels. Il a donc été enlevé par la procédure d'amincissement, ainsi chaque pixel ne touche au plus que 2 pixels. Nous pouvons maintenant lancer la procédure de conversion en vecteur en lançant le menu *r.to.vect* (:numref:`grass-qgis-lignes-vect`).

.. figure:: figures/fen_grass_qgis_lignes_to_vecteur.png
    :width: 25em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: grass-qgis-lignes-vect
    
    Conversion d'un raster de lignes en vecteur avec GRASS GIS dans QGIS.

Dans le menu ``Couche raster en entrée`` nous indiquons le raster aminci, ici *hydro_thin*. Dans ``Type d'entités`` nous précisons qu'il s'agît de lignes (*line*). Nous donnons éventuellement un nom pour le champ qui contiendra les valeurs du raster dans le menu ``Nom de la colonne d'attribut`` et enfin dans ``Vectorisé``, nous précisons un chemin et un nom pour le fichier vecteur de type lignes qui sera créé. Pour finir nous cliquons sur :guilabel:`Exécuter`. Nous avons bien un vecteur de type lignes qui apparaît.

Raster vers vecteur avec GDAL
******************************

Il est possible de faire une conversion de raster en vecteur avec GDAL en ligne de commande. Cette procédure est rapide et peut être intéressante dans une optique d'automatisation. Il est juste nécessaire de faire attention à la syntaxe qui est somme toute assez simple. Il faut, dans une console, se placer dans le répertoire qui contient le raster à convertir et entrer la commande suivante :

.. code-block:: sh

    gdal_polygonize.py xtr_usol_2011.tif -f "GPKG"  xtr_usol_2011_gdal.gpkg usol_2011 classe

avec les éléments suivants :

* *gdal_polygonize.py* : le nom de la commande GDAL à utiliser

* *xtr_usol_2011.tif* : le nom du raster à convertir

* *-f "GPKG"* : le format de la couche vecteur à créer. Mettre "SHP" pour créer un shapefile.

* *xtr_usol_2011_gdal.gpkg* : le nom du fichier vecteur qui sera créé

* *usol_2011* : le nom de la couche qui sera créé dans le geopackage. Cette option n'est possible que pour les formats vecteur proposant de stocker plusieurs couches (comme le geopackage).

* *classe* : le nom du champ qui contiendra les valeurs du raster

.. note::
	Si nous ne nous plaçons pas dans le répertoire qui contient le raster à convertir, nous devons pointer explicitement sur ce raster en inscrivant son chemin dans la ligne de commande, par exemple *C:\\Test\\Truc\\mon_raster.tif* sous Windows ou */home/SIG/truc/mon_raster.tif* sous Linux. Faire de même pour l'export du vecteur si désiré.

Raster vers vecteur avec R
******************************
Version de R : 4.8.1

Il est possible de convertir un raster vers un vecteur en utilisant R. La méthode à employer sera différente selon que le raster est stocké sous forme d'objet *raster* ou *stars*. Dans notre exemple, nous convertirons un raster au format *stars* stocké dans une variable nommée *raster_binaire* où certains pixels ont la valeur *1* et les autres la valeur *0*. En sortie, nous souhaitons obtenir un vecteur *vecteur_binaire* au format *sf*. Cette conversion se fait via la méthode ``st_as_sf()`` du package *stars*. Cette méthode prend en entrée le raster à convertir et sort un objet vecteur au format *sf*. Dans cette méthode, il est nécessaire de préciser si le vecteur produit sera sous forme de points ou non. Nous précisons également si nous souhaitons fusionner les polygones adjacents qui possèdent la même valeur, c'est souvent le cas.

.. code-block:: R

    vecteur_binaire <- st_as_sf(raster_binaire, as_points=FALSE, merge = TRUE)

La couche vecteur résultat sera une couche de polygones car nous avons mis *as_points=FALSE*. À chacun des polygones a été ajouté un attribut qui porte le même nom que le raster converti. Dans cet attribut, nous trouvons des *1* pour les polygones qui correspondent aux pixels du raster initial à *1* et des *0* pour les polygones correspondant aux pixels du raster initial à *0*.



.. |icone_actualiser| image:: figures/icone_actualiser_SCP.png
              :width: 25 px

.. |icone_browse| image:: figures/icone_browse.png
              :width: 25 px

.. _Roya: https://fr.wikipedia.org/wiki/Roya
.. _le site de OTB: https://www.orfeo-toolbox.org/CookBook/Applications/app_ZonalStatistics.html
