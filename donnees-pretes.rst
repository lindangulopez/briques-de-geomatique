..  _donnees-pretes:

Auteur : Paul Passy

Licence : |cc_by_nc_sa|

.. |cc_by_nc_sa| image:: figures/Cc-by-nc-sa_icone.png
              :width: 80 px

Données prêtes à l'emploi
===========================

Avant de se lancer dans la création d'une donnée "finie" vecteur ou raster à partir de données plus ou moins brutes, il est toujours bon de s'assurer que la donnée souhaitée n'existe pas déjà. En effet, les données plus ou moins prêtes à l'emploi existent en profusion et sont souvent librement distribuées par les producteurs. Ces données sont de tout type. Il peut s'agir de données administratives nationales ou internationales, de données relatives à l'usage du sol à différentes échelles et pour différentes régions du monde, de réseaux hydrographiques, de données historiques...

Nous verrons dans cette partie quelques données existantes et utiles au géographe.

Usage du sol
--------------

Les cartes d'usages du sol sont une des données les plus utiles en géographie et en sciences de la Terre. Même s'il est possible de créer ses propres cartes d'usages du sol, il est bon de savoir que de nombreuses données relatives aux usages du sol existent déjà. Beaucoup de ces données existent au format raster, géoréférencées, à différentes échelles, à différentes résolutions spatiales et à différentes temporalités. Ainsi, avant de se lancer dans une classification d'images satellites, il est bien de vérifier ce qui existe déjà. Selon sa zone d'étude et son objet d'étude, l'existant pourra être largement suffisant.

.. note::
	**Usage du sol ou Occupation du sol ?** Les deux termes sont souvent employés de façon interchangeable. En toute rigueur, *Occupation du sol* renvoie simplement à ce que l'on voit sur le terrain comme une *terre arable* ; alors qu'*usage du sol* renvoie vraiment à son *usage*, une terre arable sera donc vue comme *culture d'hiver*, *culture d'été*, *culture permanente*... 

Il existe des rasters d'usage du sol à toutes les échelles. Certains sont à l'échelle globale, i.e. ils couvrent l'ensemble du globe. D'autres sont à l'échelle continentale, nationale et même sub-nationale. Généralement, plus les superficies couvertes sont grandes, plus la résolution spatiale des données est grossière. Inversement, les usages du sol aux échelles nationales et sub-nationales seront à des résolutions spatiales plus fines.

Souvent, ces usages du sol sont produits par des programmes pérennes dans le temps. Ainsi, les données ne sont pas disponibles pour une seule année mais pour plusieurs millésimes plus ou moins espacés dans le temps. Par exemple, l'usage du sol européen *Corine Land Cover* est produit à peu près tous les 6 ans. L'usage du sol français *Theia* l'est tous les ans. De plus, les classes thématiques d'un usage du sol donné sont stables dans le temps. Ça signifie que la classe *Forêt*, pour un programme donnée, de la carte de 1990 est méthodologiquement identique (ou quasi) à la classe *Forêt* de la carte de 2018.

Dans cette fiche, nous présenterons les usages du sol les plus connus, en partant de l'échelle la plus petite à la plus grande.

.. contents:: Table des matières
    :local:

Europe
**********

Corine Land Cover
+++++++++++++++++++

**Présentation**

À l'échelle de l'Union Européenne (et pays voisins), il existe un usage du sol nommé `Corine Land Cover`_. C'est un usage du sol très utilisé dans de nombreux domaines. Il fait clairement référence à l'échelle de l'Europe (:numref:`clc-europe-2018`). Corine Land Cover (CLC) est produit par le programme européen *Copernicus* depuis 1990. Les années disponibles sont 1990, 2000, 2006, 2012 et 2018. Les pays couverts varient de 27, pour 1990, à 39 pour 2012 et 2018.

.. figure:: figures/fig_clc_europe_2018.png
    :width: 40em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: clc-europe-2018
    
    Corine Land Cover 2018 à l'échelle de l'Europe.

Corine Land Cover propose une classification en 44 classes d'usages du sol. Cette classification est faite en partie manuellement par interprétation visuelle ou de façon semi-automatique. Pour être individualisée, une entité doit mesurer au mois 25 ha ou être large d'au moins 100 m dans le cas d'une entité linéaire comme une rivière ou une route.

Ces rasters d'occupation du sol sont accompagnés de rasters de *changements* d'usages du sol survenus entre deux millésimes. Il y a donc un raster de *changements* 1990 - 2000, 2000 - 2006, 2006 - 2012 et 2012 - 1018. Au contraire des rasters d'usages du sol, ces rasters de changements cartographient tous les changements supérieurs à 5 ha. Ces rasters sont donc plus précis. Il est par conséquent conseillé de travailler avec ces rasters de changements lorsqu'on s'intéresse à cartographier les changements survenus entre deux dates.

.. _Corine Land Cover: https://land.copernicus.eu/pan-european/corine-land-cover

.. tip::
	Corine Land Cover étant disponible à l'échelle de l'Europe, il est facile d'obtenir le CLC pour un seul pays en découpant simplement le raster original avec le polygone du pays souhaité.

**Obtenir les données**

Les données CLC sont distribuées librement et gratuitement. Il existe différents sites de téléchargement mais le `site européen`_ est très bien fait. Outre des informations générales, nous y trouvons les différents rasters d'usages du sol et de changements (:numref:`clc-site`).

.. _site européen: https://land.copernicus.eu/pan-european/corine-land-cover

.. figure:: figures/fig_clc_site.png
    :width: 40em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: clc-site
    
    Le site européen de téléchargement des données Corine Land Cover.

Les vignettes marquées *CLC 1990* à *CLC 2018* sont les rasters d'usages du sol et celles marquées *CHA 1990-2000* à *CHA 2012-2018* sont les rasters de changements. Pour télécharger le millésime 2018, nous cliquons sur la vignette correspondante et une nouvelle fenêtre s'ouvre. Dans cette fenêtre nous disposons d'un aperçu du raster sur lequel nous pouvons nous déplacer et zoomer.

.. figure:: figures/fig_clc_site_apercu.png
    :width: 40em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: clc-site-apercu
    
    Aperçu du raster à télécharger.
 
À ce niveau, il suffit de cliquer sur *Download* pour télécharger la donnée souhaitée.
 
.. warning::
	Il est nécessaire de s'enregistrer pour pouvoir télécharger les données. Cette inscription est totalement gratuite.

Sur cette page de téléchargement, les différents formats disponibles sont présentés. Il y a un format raster GeoTiff et deux formats vecteurs ESRI Geodatabase et GeoPackage. Nous sélectionnons le format désiré en sélectionnant la case correspondante. Une fois le format sélectionné, il suffit de cliquer sur *Download* et le téléchargement se lance. Notons que les formats vecteurs sont bien plus lourds que le format raster. Selon la qualité de notre connexion le téléchargement peut être plus ou moins long.

Si nous avons choisi le format raster, nous disposons maintenant d'un fichier compressé en *.zip* présentant un nom obscur. Une fois ce fichier décompressé, nous trouvons un nouveau fichier compressé mais disposant d'un nom intelligible. Une fois ce second fichier décompressé nous accédons enfin aux données désirées.

Le raster en lui même se trouve dans le sous répertoire *DATA* et présente une extension en *.tif*. C'est celui-ci qu'il faut ouvrir dans QGIS ou tout autre logiciel. Le SCR de ce raster, comme pour toutes les données officielles européennes, est le *ETR89-extended / LAEA Europe* dont le code EPSG est *3035*. Parmi les fichiers téléchargés, dans le sous répertoire *Legend*, nous trouvons un style de légende raster pour QGIS au format *.qml*. Pour l'appliquer à notre raster, nous faisons un *clic droit* sur la couche CLC dans le panneau des couches, puis *Propriétés*. Au bas de la fenêtre, nous cliquons sur *Style* puis *Charger le style...* Nous pointons alors sur le fichier *.qml* désiré. La carte se met à jour automatiquement (:numref:`clc-iberique`).

.. figure:: figures/fig_clc_iberique.png
    :width: 20em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: clc-iberique
    
    Raster CLC avec légende.

Si nous déroulons la symbologie du raster au niveau du panneau de couches, nous pouvons maintenant voir les codes CLC et leurs intitulés en anglais (:numref:`clc-legende-anglais`).

.. figure:: figures/fig_clc_legende_anglais.png
    :width: 20em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: clc-legende-anglais
    
    La légende du raster CLC en anglais.

.. note::
	Les DOM (Guadeloupe, Martinique, Guyane, Mayotte et Réunion) sont également couverts par CLC mais dans des rasters à part. Ils se trouvent dans le répertoire *DATA* > *French DOMs*.*

**Les classes d'usages du sol**

Corine Land Cover présente un usage du sol hiérarchisé sur 3 niveaux, du plus grossier au plus fin. Les détails de ces niveaux avec une nomenclature en français et en anglais se trouve `à ce lien`_. Au premier niveau, nous trouvons les 5 usages suivants :

.. _à ce lien: https://www.statistiques.developpement-durable.gouv.fr/sites/default/files/2018-12/clc-nomenclature-c_1.xls

* *100* : *surfaces artificielles*. Nous y retrouvons tout ce qui tourne autour du bâti et de l'urbain.
* *200* : *surfaces agricoles*. Il s'agît de toutes les surfaces consacrées à l'agriculture.
* *300* : *Zones forestières et semi-naturelles*. Cette classe regroupe toutes les forêts ainsi que les zones de landes, maquis ...
* *400* : *Zones humides*. Nous y trouvons les zones humides continentales et côtières.
* *500* : *Surfaces en eau* : Il s'agît de toutes les surfaces eau, comme la mer, les rivières, les lacs...

Ensuite, chacun de ces niveaux généraux sont subdivisés en classes plus précises. Par exemple, au sein des *Zones forestières et semi-naturelles*, nous trouvons le deuxième niveau suivant :

* *310* : Forêts
* *320* : Végétations herbacées
* *330* : Espaces ouverts avec peu de végétation

Puis, au sein de ces niveaux nous trouvons le troisième et dernier niveau, le plus précis. Par exemple, au sein du niveau 310 correspondant à la forêt, nous trouvons le troisième niveau suivant :

* *311* : Forêts de feuillus
* *312* : Forêts de conifères
* *313* : Forêts mixtes

Cette hiérarchisation laisse la possibilité à l'utilisateur de s'intéresser plus ou moins finement à certains usages du sol.

**Les rasters de changements**

Comme dit précédemment, entre deux dates nous pouvons évaluer les changements survenus grâce aux rasters de changements fournis à une échelle spatiale plus fine que les données de base. Ces rasters de changement sont téléchargeables via les vignettes marquées *CHA 1990-2000* (pour les changements entre 1990 et 2000) et *CHA 2012-2018* (pour les changements entre 2012 et 2018).

Comme pour les données CLC d'usages du sol, nous pouvons télécharger ces données en format raster ou vecteur. Si nous choisissons le format raster, nous trouvons encore un fichier compressé contenant un autre fichier compressé. C'est dans ce second fichier que nous trouvons les données organisées comme pour les données CLC de base. La différence est que dans le répertoire *DATA*, nous trouvons deux rasters au format *.tif*. Le premier est nommé (dans le cas des changements survenus entre 2012 et 2018) *U2018_CHA1218_12_V2020_20u1.tif* et le second *U2018_CHA1218_18_V2020_20u1.tif*. Notons qu'un fichier de légende est également disponible dans le sous répertoire *Legend*.

Dans le premier (*U2018_CHA1218_12_V2020_20u1.tif*), nous trouvons les portions de surface qui ont changé et telles qu'elles **étaient** en 2012. C'est la couche *from* dans le jargon du *CLC changement*. Dans le second raster *U2018_CHA1218_18_V2020_20u1.tif*, nous retrouvons exactement les mêmes portions de surface mais telles qu'elles sont **devenues** en 2018. C'est la couche *to* dans le jargon du *CLC changement*. L'exemple suivant présente un zoom d'un changement entre 2012 et 2018 (:numref:`clc-change-12-18`).

.. figure:: figures/fig_clc_change_2012_2018.png
    :width: 45em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: clc-change-12-18
    
    CHA1218_12 : situation de la zone changée en 2012 ; CHA1218_18 : situation de la zone changée en 2018.

France
**********

Corine Land Cover
+++++++++++++++++++

Corine Land Cover étant une base de donnée européenne, les données couvrent également la France. Pour obtenir le raster CLC sur la France, il suffit de découper le CLC européen avec le polygone des limites de la France. Mais il est également possible de télécharger les données CLC déjà découpées pour la France sur différents sites. Sur le site du `Ministère du Développement Durable`_, nous trouvons les données au format *shapefile* par millésime pour toute la France mais également par région. Nous y trouvons également des analyses statistiques par commune. Pour des détails sur les données CLC, référez vous à la section précédente présentant Corine Land Cover au niveau européen.

.. _Ministère du Développement Durable: https://www.statistiques.developpement-durable.gouv.fr/corine-land-cover-0

Usage du sol Theia
+++++++++++++++++++++

Il existe également des données d'usage du sol plus précises dans le temps et l'espcace que Corine Land Cover. Ces données plus précises sont produites par le groupe de travail **Theia**, d'où leurs noms. Il s'agît d'usages du sol construits par classification automatique d'images satellites Sentinel-2. La résolution spatiale des rasters résultants est de 10 m, ce qui est très fin pour une échelle nationale. L'autre avantage de cet usage du sol est qu'il est produit tous les ans depuis 2016. Nous pouvons ainsi suivre finement les tendances de transformations des usages du sol nationaux dans le temps. Les données sont proposées au format raster à l'échelle de la Métropole et au format vecteur par département (:numref:`theia-france`). 

Le nombre de classes représentées augmente au fil des années. En 2018 et 2019, 23 classes sont disponibles et entre 2016 et 2017, 17 classes sont disponibles.

La méthode de classification utilisée est suffisamment robuste pour l'appliquer sur des images satellites autres que Sentinel-2. Ainsi, en utilisant des images Landsat 5 et Landsat 8, il a été possible de produire des millésimes historiques pour les années 2009, 2010, 2011 (avec Landsat 5) et 2014 (avec Landsat 8). Ces millésimes historiques sont en 17 classes d'usage du sol avec une résolution spatiale de 30 mètres. La typologie des classes ainsi que leurs correspondances temporelles est disponible ici :download:`disponible ici <_static/usol_theia_classes.ods>`.

.. figure:: figures/fig_theia_France_2019.png
    :width: 30em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: theia-france
    
    Usage du sol Theia pour la France en 2019.

Les données d'usage du sol pour la période 2016 - 2018 sont disponibles sur le `site du CESBIO`_, le laboratoire qui produit ces données. Nous y trouvons des informations sur la production des données et les méthodes utilisées pour leur production. Plus précisément, vous trouverez les différents millésimes aux liens suivants :

* `raster 2018`_ et style 2018 :download:`style 2018 <_static/OCS_2018_CESBIO.qml>`
* `raster 2017`_ et style 2017 :download:`style 2017 <_static/OCS_2017_CESBIO.qml>`
* `vecteur 2017 par département`_
* `raster 2016`_ et style 2016 :download:`style 2016 <_static/OCS_2016_CESBIO.qml>`
* `vecteur 2016 par département`_

.. _site du CESBIO: http://osr-cesbio.ups-tlse.fr/oso/
.. _raster 2018: http://osr-cesbio.ups-tlse.fr/echangeswww/TheiaOSO/OCS_2018_CESBIO.tif
.. _raster 2017: http://osr-cesbio.ups-tlse.fr/echangeswww/TheiaOSO/OCS_2017_CESBIO.tif
.. _vecteur 2017 par département: http://osr-cesbio.ups-tlse.fr/echangeswww/TheiaOSO/vecteurs_2017/liste_vecteurs.html
.. _raster 2016: http://osr-cesbio.ups-tlse.fr/echangeswww/TheiaOSO/OCS_2016_CESBIO.tif
.. _vecteur 2016 par département: http://osr-cesbio.ups-tlse.fr/echangeswww/TheiaOSO/vecteurs_2016/liste_vecteurs.html

Les données historiques 2009 - 2014 sont disponibles au format raster sur cet `autre page du CESBIO`_ et le fichier de style correspondant peut être trouvé ici :download:`ici <_static/OCS_2009-2014_CESBIO.qml>` (clic droit *enregistrer le lien sous*).

.. _autre page du CESBIO: http://osr-cesbio.ups-tlse.fr/~oso/posts/2016-10-06-cartes-2009-2011/

.. tip::
	Une spécificité de ces cartes d'usages du sol Theia est d'être fournies avec un raster de *confiance*. Ainsi, pour chaque pixel du raster il est possible de connaître sa marge d'erreur de classification.
