..  _geomorpho:

Auteur : Paul Passy

Licence : |cc_by_nc_sa|

.. |cc_by_nc_sa| image:: figures/Cc-by-nc-sa_icone.png
              :width: 80 px

Calculs géomorphologiques
==========================

Il est souvent utile en géographie de dériver des données d'intérêt géomorphologique à partir de modèles numériques de terrain (MNT). À partir d'un MNT, il est notamment possible de calculer un raster de pentes.

.. contents:: Table des matières
    :local:

Calcul de pentes
-------------------

Calculer des pentes à partir d'un modèle numérique de terrain (MNT) est une manipulation très courante. La plupart des outils de SIG intègrent une fonction permettant de faire ce calcul. À la fin de ce calcul, nous obtenons un raster, de même dimension que le MNT initial, mais dans lequel les pixels ne représentent plus une valeur d'élévation mais une valeur de pente locale. Cette pente peut être exprimée en degrés ou en pourcentage. Le choix de l'unité est en général laissé à la discrétion de l'utilisateur.

Dans les exemples suivants, nous allons calculer les pentes du bassin-versant de la `Vésubie`_ (Alpes-Maritimes) à partir du MNT local issu du SRTM ("*srtm_vesubie.tif*"). 

Calcul de pentes avec QGIS
***************************

Version de QGIS : 3.16.1

Dans un premier il est nécessaire de charger ce MNT dans QGIS. Une fois ce raster chargé, nous allons dans le menu :menuselection:`Raster --> Analyse --> Pente...`       *Raster* > *Analyse* > *Pente...*. La fenêtre suivante s'affiche (:numref:`qgis-pentes`).

.. _Vésubie: https://fr.wikipedia.org/wiki/V%C3%A9subie

.. figure:: figures/fen_calcul_pentes_qgis.png
    :width: 30em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: qgis-pentes
    
    Paramétrage du calcul de pentes dans QGIS.

Dans le champ ``Couche source``, nous spécifions le MNT à utiliser, ici *srtm_vesubie*. Dans le champ ``Pente``, nous spécifions un chemin d’export de notre raster de pentes. Les autres options peuvent être laissées telles quelles. En cochant la case ``Pente exprimée en pourcentage plutôt qu'en degrés`` permet d'obtenir un raster de pentes exprimés en pourcentage. Nous pouvons également constater que cet outil est en fait un simple interface à une fonction GDAL, comme nous pouvons le voir dans le panneau ``Console GDAL/OGR``. Le raster de pentes apparaît ensuite automatiquement dans QGIS.
