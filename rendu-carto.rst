..  _rendu-carto:

Auteur : Paul Passy

Licence : |cc_by_nc_sa|

.. |cc_by_nc_sa| image:: figures/Cc-by-nc-sa_icone.png
              :width: 80 px

Rendu cartographique et visualisation
======================================

Le rendu cartographique est souvent un des produits finaux d'un projet géomatique faisant intervenir des SIG ou de la télédétection. Nous allons voir ici la façon de procéder pour réaliser quelques rendus cartographiques (et autres). Il ne s'agît pas de faire un cours de cartographie, nous verrons des rendus relativement simples. Reportez vous à des manuels de cartographie pure pour de plus amples détails et à des documentations spécialisées pour les traitements avancés.

Visualisation 3D
------------------

Lorsque nous disposons d'une couche géographique possédant une valeur en *z* comme une valeur d'altitude, il est possible de créer une visualisation en 3D de nos données. C'est notamment le cas lorsque nous disposons d'un modèle numérique de terrain (MNT). Il est possible de visualiser le terrain comme si nous le survolions. Nous pouvons même y superposer des couches vecteurs ou des couches rasters pour mieux appréhender notre zone d'études.

.. note::
	Il est abusif de parler de 3D dans la plupart des cas. En toute rigueur, nous devrions parler de visualisation en 2.5D. En effet, un MNT reste un simple plan. Les pixels le constituant ne sont pas en trois dimensions. Nous parlons alors de 2.5D car la vue que nous créons s'apparente plus à une feuille de papier froissé qu'à un vrai pavé en trois dimensions.

Dans les exemples suivants, nous allons visualiser en 3D le `Mont Elbrouz`_, le plus haut sommet d'Europe, situé dans la chaîne du Causase en Russie. Le MNT utilisé sera le SRTM. Par dessus, nous y calquerons une composition colorée en vraies couleurs Landsat 8 de septembre 2020.

.. _Mont Elbrouz: https://fr.wikipedia.org/wiki/Elbrouz

.. note::
	La visualisation 3D aide à mieux appréhender son terrain d'études et présente une sortie *sexy*. Mais d'un point de vue strictement scientifique, elle n'apporte pas grand chose.

.. contents:: Table des matières
    :local:

Visualisation 3D dans QGIS avec l'extension QGIS2threejs
*********************************************************
Version de QGIS : 3.18.0

Il existe deux façons de créer des vues 3D dans QGIS. La plus aboutie est celle utilisant l'extension nommée QGIS2threejs. Cette extension est basée sur la technologie ̀ threejs`_. Il s'agît d'une extension Javascript permettant d'afficher et de manipuler des objets en 3D dans une page web. Ainsi, grâce à cette extension il sera possible de créer une vue 3D au sein de QGIS mais également d'exporter cette vue dans un fichier HTML. Ce type d'export nous permettra d'explorer et de partager notre vue avec un simple navigateur web sans avoir besoin de QGIS.

Pour commencer, il est nécessaire d'installer l'extension en allant dans le menu *Extensions* et en cherchant l'extension par son nom *QGIS2threejs*. Une fois installée, elle est accessible via le menu *Internet* > *QGIS2threejs*.

Nous chargeons maintenant notre MNT de la zone *SRTM_Elbrouz.tif*. Une fois ce raster chargé, nous ouvrons le menu principal de QGIS2threejs. La fenêtre principale de l'extension apparaît (:numref:`3d-threejs`). Dans le panneau *Layers*, dans le menu *DEM*, nous cochons notre MNT, à savoir *SRTM_Elbrouz*. Nous spécifions ainsi que le MNT qui sert de support à la 3d est ce raster ci.

.. _threejs: https://threejs.org/

.. figure:: figures/fen_threejs_init.png
    :width: 45em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: 3d-threejs
    
    Initialisation de la vue 3D.

Le MNT apparaît avec la même symbologie que celle utilisée dans QGIS. Par défaut il s'agît d'un dégradé de gris, avec les altitudes les plus hautes qui tendent vers le blanc et les plus basses vers le noir. En allant dans le menu *Widget*, il est possible d'afficher le repère x, y, et z (*Navigation Widget*) en bas à droite et la flèche du nord (*North Arrow*).

Il est possible de régler l’exagération verticale, i.e. de l'altitude. Par défaut, il n'y en a pas, elle est à *1*. Sur l'exemple précédent, elle est réglée à *2*, ce qui permet d'accentuer le relief. Pour cela, nous allons dans le menu *Scene* > *Scene Settings...* et dans le menu qui s'ouvre nous pouvons mettre la *Vertical exaggeration* à *2*.

Superposer un raster par dessus est très simple. Il suffit de charger le raster d'intérêt, ici *LC08_20200927_Elbrouz.tif*, dans QGIS et de le mettre au-dessus du raster dans le panneau de couches. La vue 3D se met à jour automatiquement. Notons, que si nous disposions de fichiers vecteurs, il serait possible de les ajouter de la même manière. Nous pourrions également gérer leur affichage via les menus *Line*, *Polygon* et *Point Cloud* du panneau *Layers* (:numref:`3d-threejs-landsat`).

.. figure:: figures/fen_threejs_Landsat.png
    :width: 45em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: 3d-threejs-landsat
    
    Vue 3D avec une composition colorée Landsat superposée.

Il est possible de zoomer et de dézoomer dans cette vue 3D à l'aide de la molette de la souris. Le changement d'inclinaison et d'orientation de la vue se fait à l'aide du curseur de la souris. Pour changer le centre de la vue, cela se fait également via le curseur de la souris mais en maintenant enfoncée la touche *Control*. Lors des mouvements, la flèche du Nord pointe toujours vers le nord et le repère de navigation suit le mouvement général (:numref:`3d-threejs-orientation`).

.. figure:: figures/fen_threejs_orientation.png
    :width: 45em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: 3d-threejs-orientation
    
    Changement de position de la vue 3D.

Il est possible d'exporter votre vue 3D en image PNG via le menu *File* > *Save scene as...* > *Image (.png)*. Il est également possible d'exporter votre vue 3D en un vrai fichier 3D en choisissant le format *glTF (.gltf, .glb)*. Ce format est à la 3D ce que le Jpeg est à la 2D. Vous pourrez par exemple importer votre vue dans un logiciel de 3D comme Blender.

Une force de cette extension est de pouvoir exporter les vues 3D dans un fichier HTML manipulable facilement avec n'importe quel navigateur web. Pour cela, il suffit d'aller dans *File* > *Export to Web...* La fenêtre d'export s'ouvre alors (:numref:`3d-threejs-export`).

.. figure:: figures/fen_threejs_export.png
    :width: 28em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: 3d-threejs-export
    
    Export de la vue 3D pour le Web.

Dans le champ *Output Directory* nous spécifions un répertoire d'export. Dans le champ *HTML filename*, nous indiquons un nom pour le fichier HTML principal, ici *Elbrouz.html*. Nous pouvons indiquer un titre pour la page Web dans le champ *Page Title*. Il est conseillé de cocher la case *Enable the Viewer to Run Locally* pour éviter certains bugs d'affichage. Puis nous cliquons sur *Export*.

Dans notre explorateur de fichier, nous n'avons plus qu'à ouvrir le fichier *Elbrouz.html* avec un navigateur pour explorer notre vue 3D indépendamment de QGIS.

.. note::
	**Au final**, cette solution est vraiment très paratique. Le rendu est bon, fluide et rapide. L'intégration dans QGIS est très bonne et la navigation dans la vue 3D facile. La possibilité d'exporter en HTML est également très appréciable, je pense que c'est la meilleure solution pour de la visualisation 3D dans QGIS.

Visualisation avec la Vue Cartographique 3D de QGIS
*********************************************************
Version de QGIS : 3.18.0

Il existe dans QGIS, une façon native de visualiser les données en 3D sans passer par des extensions tierces. Cette visualisation se fait à l'aide de la *Vue Cartographique 3D*.

Une fois notre MNT chargé, nous pouvons ouvrir cette vue 3D via le menu *Vue* > *Nouvelle Vue Cartographique 3D*. La vue fenêtre de la vue 3D s'affiche alors. C'est une fenêtre flottante qu'il est possible "d'accrocher" à un des bords de la fenêtre principale de QGIS (:numref:`3d-qgis`).

.. figure:: figures/fen_vue_3D_qgis.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: 3d-qgis
    
    Vue Cartographique 3D de QGIS.

La première chose à faire est de définir le MNT qui va servir de support à la 3D. Pour cela, nous allons dans le menu *Options* sous l'icône |icone_options| et nous sélectionnons *Configurer* (:numref:`3d-qgis-options`).

.. figure:: figures/fen_vue_3D_qgis_options.png
    :width: 30em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: 3d-qgis-options
    
    Réglage des options de la vue 3D dans QGIS.

Dans le champ *Type* nous spécifions que nous souhaitons représenter un *MNT (couche Raster)*. Dans le champ *Élévation*, nous indiquons le MNT à représenter, à savoir *SRTM_Elbrouz*. Nous pouvons régler l'échelle verticale, qui est en fait l'exagération verticale. Nous pouvons la mettre à *2*. Puis nous cliquons sur *OK*.

Le MNT est vu d'en haut, il faut maintenant régler l'angle de vue pour profiter de la 3D. Pour cela, nous cliquons n'importe où sur la vue 3D et nous bougeons le curseur de la souris en maintenant la touche *Majuscule* enfoncée. Il est aussi possible de zoomer et dézoomer via la molette de la souris. Nous pouvons également déplacer la vue avec la souris comme nous le ferions du raster dans la fenêtre principale de QGIS (:numref:`3d-qgis-orientation`).

.. figure:: figures/fen_vue_3D_qgis_orientation.png
    :width: 30em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: 3d-qgis-orientation
    
    Réglage de l'angle de vue.

La superposition d'une autre couche se fait simplement en mettant une autre couche par dessus le MNT dans le panneau des couches dans QGIS. En ce qui concerne l'affichage des couches, la vue 3D se comporte comme la vue normale de QGIS. Ici, nous superposons la composition colorée Landsat 8 de septembre 2020 *LC08_20200927_Elbrouz.tif* (:numref:`3d-qgis-landsat`).

.. figure:: figures/fen_vue_3D_qgis_Landsat.png
    :width: 30em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: 3d-qgis-landsat
    
    Superposition d'une composition colorée vraies couleurs Landsat 8.

Il est possible d'exporter une image de sa vue 3D via l'icône |icone_export_image| *Enregistrer comme image...*. Nous pouvons également exporter notre vue en objet 3D *.obj* manipulable par d'autres logiciels en cliquant sur l'icône |icone_export_objet| *Export 3D Scene*.

.. |icone_options| image:: figures/icone_3d_options.png
              :width: 25 px

.. |icone_export_image| image:: figures/icone_export_3D_image.png
              :width: 20 px

.. |icone_export_objet| image:: figures/icone_export_3D_obj.png
              :width: 20 px


Créer une animation ou une vidéo
**********************************
Il est possible d'exporter une série de vues au format Jpeg afin de les assembler dans un GIF animé ou dans une vidéo. Au final nous obtenons une vidéo d'un survol de notre vue 3D. Pour cela nous ouvrons le panneau d'animation en cliquant sur l'icône *Animations* |icone_export_animation|. Le panneau d'animation s'ouvre en bas de la fenêtre. Par défaut, la vidéo débutera avec la vue qui apparaît au moment de l'ouverture du menu d'animations.

.. |icone_export_animation| image:: figures/icone_export_3D_animation.png
              :width: 17 px

Nous changeons maintenant d'angle de vue, à l'aide du curseur de la souris, pour avoir notre deuxième vue de la vidéo. Lorsque nous somme satisfaits de notre deuxième vue, nous cliquons sur *Ajouter une image clé* |icone_export_add|. Une fenêtre nous demande la position temporelle de cette vue dans l'animation, ici nous indiquons que cette vue doit apparaître après *1* seconde de vidéo (:numref:`3d-frame`).

.. figure:: figures/fen_export_3D_video_frame.png
    :width: 14em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: 3d-frame
    
    Position temporelle de la vue.

.. |icone_export_add| image:: figures/icone_export_3D_add_frame.png
              :width: 17 px

Nous répétons ensuite cette opération avec autant d'étapes que désirées. Par défaut, la vidéo dure 5 secondes, il est possible de faire une vue toutes les secondes. Ainsi, nous répétons cette étape avec des vues différentes aux positions 2, 3 et 4 secondes. La position de la seconde 5 est déjà fixée par défaut. Mais à l'aide de l'édition des images clés, il est possible de la modifier.

Il faut bien noter, que ce menu d'animations fera des interpolations d'images entre nos différentes vue afin d'obtenir une vidéo fluide. Il est possible de régler la nature de l'interpolation dans le menu *Interpolation*. Avant d'exporter notre vidéo, nous pouvons la jouer pour en juger la qualité. Il suffit de cliquer sur l'icône |icone_export_animation| en bas. Si nous sommes satisfaits, nous exportons les images de l'animation en cliquant sur *Exporter les images de l'animation* |icone_enregistrer|. Il faut bien noter, que nous n'exportons pas la vidéo directement mais seulement les images la constituant. C'est pourquoi la fenêtre d'export des images s'affiche (:numref:`3d-export-images`).

.. figure:: figures/fen_export_3D_images.png
    :width: 23em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: 3d-export-images
    
    Paramètres d'export des images de l'animation.

.. |icone_enregistrer| image:: figures/icone_3D_enregistrer.png
              :width: 17 px

Nous ne touchons pas au champ *Modèle*. Nous sélectionnons juste un *Répertoire de destination*. Les autres réglages peuvent être laissés à leurs valeurs par défaut. Puis nous cliquons sur *OK*.

.. note::
	Changer la nature de l'interpolation des images ne semble pas changer drastiquement le rendu.

Si nous regardons dans notre explorateur de fichiers le répertoire créé, nous constatons que plusieurs dizaines d'images *.jpg* ont été exportées. Seules 5 de ces images correspondent aux vues que nous avons définies, les autres résultent de l'interpolation. C'est maintenant à l'utilisateur de créer son animation à partir de ces images. Il est possible de créer un GIF animé, pour les nostalgiques des années 90, ou un fichier vidéo en format *.avi*, *.mp4* ou autre.

Pour un GIF animé, il est possible d'utiliser une solution en ligne ou le logiciel ̀ ImageMagick`_ (disponible sous Linux et Windows) qui s'utilise en lignes de commandes. Une fois ce logiciel installé, nous nous positionnons dans le répertoire contenant les images et nous y ouvrons une invite de commandes. Nous pouvons entrer la commande suivante.

.. _ImageMagick: https://imagemagick.org/index.php

.. code-block:: sh

   convert *.jpg anim.gif

*convert* est le mot clé de ImageMagick pour convertir le format des images, ici de *.jpg* à *.gif*. *\*.jpg* signifie que nous prenons toutes les images du *Jpeg* du répertoire. Et *anim.gif* signifie que nous les assemblons dans un fichier GIF que nous nommons *anim*. Au bout de quelques secondes, nous obtenons notre GIF animé.

Pour créer une vidéo, nous pouvons utiliser l'utilitaire `ffmpeg`_ qui s'utilise également en ligne de commandes. Comme précédemment, nous nous positionnons dans le répertoire contenant les images, nous ouvrons une invite de commandes et nous pouvons y entrer la commande suivante.

.. _ffmpeg: https://ffmpeg.org/

.. code-block:: sh

   ffmpeg -framerate 25 -pattern_type glob -i '*.jpg' -c:v libx264 Elbrouz.mp4

Pour les intéressés par les options de *ffmpeg* regardez la documentation sinon acceptons la commande telle quelle. Nous obtenons ici une vidéo *.mp4* nommée *Elbrouz.mp4*. Notons qu'elle pèse bien moins lourd que le GIF animé.

.. raw:: html
   
   <video width="400" controls>
   <source src="_static//Elbrouz.mp4" type="video/mp4">
   Sorry, your browser doesn't support embedded videos.
   </video> 
