..  _geometries-vecteur:

Auteur : Paul Passy

Licence : |cc_by_nc_sa|

.. |cc_by_nc_sa| image:: figures/Cc-by-nc-sa_icone.png
              :width: 80 px

Travailler sur les géométries vecteur
========================================

Dans ce chapitre vous trouverez quelques outils dédiés à la manipulation des géométries des couches vectorielles.

.. contents:: Table des matières
    :local:

..  _extract-vertex:

Extraction des vertex
--------------------------

Les polygones et les polylignes sont en fait constitués d'une série de points, appelés *vertex* reliés entre eux par des morceaux de lignes. Dans le cas des polygones, ces morceaux de lignes sont fermés et une surface est comprise dans ce périmètre. Dans certains cas, il est intéressant d'extraire les vertex constitutifs des lignes ou des polygones. Cette manipulation se fait plus ou moins facilement selon les outils.

Dans les exemples suivants, nous allons voir comment extraire les vertex constitutifs du polygone présenté sur la figure suivante (:numref:`polygone-vertex`).

.. figure:: figures/fig_polygone_vertex.png
    :width: 30em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: polygone-vertex
    
    Vertex constitutifs d'un polygone.

..  _extract-vertex-qgis:

Extraction avec QGIS
*********************
Version de QGIS : 3.22.0

L'outil à utiliser se trouve dans le menu :menuselection:`Boîte à outils de traitements --> Géométrie vectorielle --> Extraire les sommets`. Le menu suivant s'ouvre.

.. figure:: figures/fen_extraire_vertex_qgis.png
    :width: 25em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: xtr-vertex-qgis
    
    Extraction des vertex (aussi appelés *sommets*) avec QGIS.

Dans le menu ``Couche source`` nous indiquons le nom de la couche de polygone dont il faut extraire les sommets. Puis à la ligne ``Sommets`` nous indiquons un chemin et un nom pour la couche qui contiendra les sommets. Notons que si nous le souhaitons nous pouvons cocher la case ``Entité(s) sélectionnée(s) uniquement`` si seuls les sommets du ou des polygones sélectionnés sont nécessaires. À la fin du traitement nous obtenons une couche vectorielle de type *points* dans laquelle chaque point correspond à un sommet du polygone.
